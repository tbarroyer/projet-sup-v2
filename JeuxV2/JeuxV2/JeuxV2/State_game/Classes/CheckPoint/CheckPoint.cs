﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.CheckPoint
{
    class CheckPoint : Objet
    {
        Texture2D _sprite2;

        public void Init()
        {

        }

        public CheckPoint(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40 + 10);
        }

        public void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/CheckPoint/Sprite1");
            _sprite2 = content.Load<Texture2D>("Sprites/CheckPoint/Sprite2");
        }

        public bool Update(Player perso)
        {
            if (collision_with_other_obj(perso) != 0 && SpriteActuel != _sprite2)
            {
                Action(perso);
                return true;
            }
            return false;
        }

        public bool Update(Player perso1, Player perso2)
        {
            if ((collision_with_other_obj(perso1) != 0 || collision_with_other_obj(perso2) != 0) && SpriteActuel != _sprite2)
            {
                Action(perso1);
                Action(perso2);
                return true;
            }
            return false;
        }

        public void Action(Player perso)
        {
            SpriteActuel = _sprite2;

            perso.CheckpointX = (int)perso.PosiSprite.X;
            perso.CheckpointY = (int)perso.PosiSprite.Y;
            perso.CheckpointCoins = perso.Nbpieces;
            perso.CheckpointGemmes = perso.Nbgemmes;
        }
    }
}
