﻿using System.Collections.Generic;
using System.Linq;
using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.CheckPoint
{
    public class CheckpointManager
    {
        private readonly List<CheckPoint> _p1 = new List<CheckPoint>();
        private int _numberPuDraw;
        readonly ContentManager _content;

        public CheckpointManager(ContentManager content)
        {
            _content = content;
        }

        public void Update(Map.Map map, Player perso)
        {
            for (int i = 0; i < (int)_p1.LongCount(); i++)
            {
                _p1[i].Update(perso);
            }
        }

        public void Update(Map.Map map, Player perso1, Player perso2)
        {
            for (int i = 0; i < (int)_p1.LongCount(); i++)
            {
                _p1[i].Update(perso1, perso2);
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            for (int i = 0; i < (int)_p1.LongCount(); i++)
            {
                _p1[i].Draw(spriteBatch, gameTime, map); //Drawing the monster
            }
        }

        public void AddACheckPoint(int i, int j)
        {
            _p1.Add(new CheckPoint(i, j));

            _p1[_numberPuDraw].Init();
            _p1[_numberPuDraw].LoadContent(_content);

            _numberPuDraw++;
        }

        public void delete_all()
        {
            _p1.Clear();
            _numberPuDraw = 0;
        }

    }
}