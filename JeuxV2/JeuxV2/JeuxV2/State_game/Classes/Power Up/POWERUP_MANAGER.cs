﻿using System.Collections.Generic;
using System.Linq;
using JeuxV2.State_game.Classes.ALL;
using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes
{
    public class PowerupManager
    {
        private readonly List<PowerUp> _p1 = new List<PowerUp>();
        private int _numberPuDraw;
        readonly ContentManager _content;

        public PowerupManager(ContentManager content)
        {
            _content = content;
        }

        public void Update(Map.Map map, Player perso)
        {
            for (int i = 0; i < (int)_p1.LongCount(); i++)
            {
                if (_p1[i].Update(perso))
                {
                    _p1.RemoveAt(i);
                    _numberPuDraw--;
                }
            }
        }

        public void Update(Map.Map map, Player perso1, Player perso2)
        {
            for (int i = 0; i < (int)_p1.LongCount(); i++)
            {
                if (_p1[i].Update(perso1) || _p1[i].Update(perso2))
                {
                    _p1.RemoveAt(i);
                    _numberPuDraw--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            for (var i = 0; i < (int)_p1.LongCount(); i++)
            {
                _p1[i].Draw(spriteBatch, gameTime, map); //Drawing the monster
            }
        }

        public void AddAPowerUp(int i, int j, int type)
        {
            switch (type)
            {
                case 1:
                    _p1.Add(new PuNewLife(i, j));
                    break;
                case 2:
                    _p1.Add(new PuMegaspeed(i, j));
                    break;
                case 3:
                    _p1.Add(new PuGravity(i, j));
                    break;
                case 4:
                    _p1.Add(new PuCoin(i, j, _content));
                    break;
            }

            _p1[_numberPuDraw].Init();
            _p1[_numberPuDraw].LoadContent(_content);

            _numberPuDraw++;
        }

        public void delete_all()
        {
            _p1.Clear();
            _numberPuDraw = 0;
        }
        
    }
}