﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework.Content;

namespace JeuxV2.State_game.Classes
{
    class PowerUp : Objet
    {
        public virtual void Init()
        {

        }

        public virtual void LoadContent(ContentManager content)
        {

        }

        public virtual bool Update(Player perso)
        {
            if (collision_with_other_obj(perso) != 0)
            {
                Action(perso);
                return true;
            }
            return false;
        }

        public virtual void Action(Player perso)
        {

        }
    }
}
