﻿using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.ALL
{
    class PuGravity : PowerUp
    {
        public PuGravity(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40 + 18); //+1: FIXME
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/Power Up/power2");
        }

        public override void Action(Player perso)
        {
            perso.Nbgemmes++;
        }
    }
}
