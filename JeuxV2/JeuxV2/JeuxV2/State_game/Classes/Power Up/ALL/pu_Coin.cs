﻿using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace JeuxV2.State_game.Classes.ALL
{
    class PuCoin : PowerUp
    {
        Texture2D _sprite1;
        Texture2D _sprite2;
        Texture2D _sprite3;
        Texture2D _sprite4;

        SoundEffect song;

        bool _coinGiven;

        private int _annimpris;

        private int _anim;

        public PuCoin(int posx, int posy, ContentManager content)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40);
            _anim = 0;
            _annimpris = 0;
            song = content.Load<SoundEffect>("Sound/Coineffects");
            _coinGiven = false;
        }

        public override void LoadContent(ContentManager content)
        {
            _sprite1 = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin1");
            _sprite2 = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin2");
            _sprite3 = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin3");
            _sprite4 = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin4");

            SpriteActuel = _sprite1;
        }

        public override bool Update(Player perso)
        {
            Annim();

            if (collision_with_other_obj(perso) != 0 && !_coinGiven)
            {
                Action(perso);
            }

            return _annimpris == 20;
        }

        public override void Action(Player perso)
        {
            perso.add_coins(1);
            _coinGiven = true;
            song.Play();
        }


        private void Annim()
        {
            _anim++;

            if (_anim < 8)
                SpriteActuel = _sprite1;
            else if (_anim < 16)
                SpriteActuel = _sprite2;
            else if (_anim < 24)
                SpriteActuel = _sprite3;
            else if (_anim < 32)
                SpriteActuel = _sprite4;
            else _anim = 0;

            if (_coinGiven)
            {
                PosiSprite.Y -= 4;
                _annimpris++;
            }
        }
    }
}
