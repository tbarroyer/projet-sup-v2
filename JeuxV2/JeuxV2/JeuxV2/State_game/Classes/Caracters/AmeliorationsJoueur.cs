﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeuxV2.State_game.Classes.Caracters
{
    public class AmeliorationsJoueur
    {
        private int speed;
        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        private int typeJoueur;
        public int TypeJoueur
        {
            get { return typeJoueur; }
            set { typeJoueur = value; }
        }

        private bool doubleSaut;
        public bool DoubleSaut
        {
            get { return doubleSaut; }
            set { doubleSaut = value; }
        }

        private bool propulseur;
        public bool Propulseur
        {
            get { return propulseur; }
            set { propulseur = value; }
        }

        private bool shield;
        public bool Shield
        {
            get { return shield; }
            set { shield = value; }
        }

        private bool dblaser;
        public bool Dblaser
        {
            get { return dblaser; }
            set { dblaser = value; }
        }


        public AmeliorationsJoueur()
        {
            speed = 5;
            shield = false;
            DoubleSaut = false;
            propulseur = false;
            dblaser = false;
        }

    }
}
