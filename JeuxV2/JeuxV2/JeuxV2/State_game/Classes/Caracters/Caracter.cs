﻿using System;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using JeuxV2.Sauvegardes;

namespace JeuxV2.State_game.Classes.Caracters
{
    public class Player : Objet
    {
        public enum Clavier
        {
            Clavier1J,
            Clavier2J1,
            Clavier2J2
        }

        //MEMBERS

        #region sprites
        protected Texture2D SpriteNormal;
        protected Texture2D SpriteNormal2;
        protected Texture2D SpriteNormal3;

        protected int Countanim;

        protected Texture2D SpriteLeft1;
        protected Texture2D SpriteLeft2;
        protected Texture2D SpriteLeft3;
        protected Texture2D SpriteLeft4;

        protected Texture2D SpriteRight1;
        protected Texture2D SpriteRight2;
        protected Texture2D SpriteRight3;
        protected Texture2D SpriteRight4;

        protected Texture2D SpriteJumpR;
        protected Texture2D SpriteJumpL;
        #endregion

        protected Clavier TypeClavier;

        protected KeyboardState Ks;
        protected KeyboardState OKs;

        protected int Speed;

        #region checkpoint
        public int CheckpointX;
        public int CheckpointY;
        public int CheckpointCoins;
        public int CheckpointGemmes;
        #endregion

        public int Nbpieces;
        public int Nbgemmes;

        protected int Life;

        public bool IsInvicible;
        protected int IsInvicibleCount;

        private int countl;
        private int countr;

        #region particule
        protected struct particule
        {
            public Vector2 pos;
            public int compt;
        }
        protected Texture2D imgParticule;
        protected particule[] myParticulesTab;
        protected bool isV;
        #endregion

        //=====================================BASE======================

        public virtual void Init(Clavier type)
        {
            TypeClavier = type;

            InJump = false;
            JumpCount = 0;
            OnGround = false;
            Gravity = 5;

            Nbpieces = 0;

            IsInvicible = false;

            CheckpointCoins = 0;
            CheckpointGemmes = 0;
            Countanim = 0;
            Life = 3;

            Nbgemmes = 0;

            countr = 0;
            countl = 0;

            Speed = SauvegardesManager.LoadCurrentSave().perso.Speed;

            sautD = false;

            myParticulesTab = new particule[30];

            Random r = new Random();

            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt = r.Next(30);
                myParticulesTab[i].pos = new Vector2();
                myParticulesTab[i].pos.X = PosiSpriteEcran.X + r.Next(-10, 10);
                myParticulesTab[i].pos.Y = PosiSpriteEcran.Y + r.Next(10);
            }

            isV = false;

            v_y = -7;
        }

        public virtual void LoadContent(ContentManager content)
        {
            imgParticule = content.Load<Texture2D>("Explosions/particule");
        }

        public void Update(Map.Map map, GameComponentManager mana)
        {
            Animation();

            Invicible();

            entrées_Clavier(map);

            apply_gravity();
            collisionY_with_map(map);

            verif_mort_sortie_map(map, mana);

            UpdatePart();
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            set_posi_sprite_ecran(map);

            if (IsInvicibleCount % 2 == 0)
            {
                spriteBatch.Begin();
                if (Gravity >= 0)
                    spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, Color.White);
                else
                    spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, null, Color.White, 0, new Vector2(0), 1, SpriteEffects.FlipVertically, 0);

                if (isV)
                {
                    foreach (var particule in myParticulesTab)
                    {
                        spriteBatch.Draw(imgParticule, particule.pos, Color.White);
                    }
                }
                spriteBatch.End();
            }
        }

        //======================================PRIVATES=================

        private void entrées_Clavier(Map.Map map)
        {
            Ks = Keyboard.GetState();

            #region Clavier1J
            if (TypeClavier == Clavier.Clavier1J)
            {

                if (Ks.IsKeyDown(Keys.Left) && Ks.IsKeyUp(Keys.Right)) //GO LEFT
                {
                    countl++;

                    if (countl == 40)
                        countl = 0;

                    if (countl < 10)
                        SpriteActuel = SpriteLeft1;
                    else if (countl < 20)
                        SpriteActuel = SpriteLeft2;
                    else if (countl < 30)
                        SpriteActuel = SpriteLeft3;
                    else if (countl < 40)
                        SpriteActuel = SpriteLeft4;


                    PosiSprite.X -= Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(-Speed);
                                break;
                            case 2:
                                map.Scroll(-Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpL;
                }

                else if (Ks.IsKeyDown(Keys.Right) && Ks.IsKeyUp(Keys.Left)) //GO RIGHT
                {
                    countr++;

                    if (countr == 40)
                        countr = 0;

                    if (countr < 10)
                        SpriteActuel = SpriteRight1;
                    else if (countr < 20)
                        SpriteActuel = SpriteRight2;
                    else if (countr < 30)
                        SpriteActuel = SpriteRight3;
                    else if (countr < 40)
                        SpriteActuel = SpriteRight4;
                    PosiSprite.X += Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(Speed);
                                break;
                            case 1:
                                map.Scroll(Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpR;
                }

                if (Ks.IsKeyDown(Keys.Space) && OnGround) //JUMP
                {
                    InJump = true;
                    sautD = false;
                    if (Gravity > 0)
                        v_y = -7;
                    else v_y = 7;
                }

                if (Ks.IsKeyDown(Keys.Space) && OKs.IsKeyUp(Keys.Space) && !OnGround &&
                    SauvegardesManager.LoadCurrentSave().perso.DoubleSaut && !sautD && JumpCount >= 30)
                {
                    InJump = true;
                    JumpCount = 10;
                    sautD = true;

                    if (Gravity > 0)
                        v_y = -5;
                    else v_y = 5;
                }

                if (Ks.IsKeyDown(Keys.V) && !OnGround && !InJump &&
                    SauvegardesManager.LoadCurrentSave().perso.Propulseur)
                {
                    if (Gravity > 0)
                        Gravity = 2;
                    else Gravity = -2;
                    isV = true;
                }
                else
                {
                    if (Gravity > 0)
                        Gravity = 5;
                    else Gravity = -5;
                    isV = false;
                }

                if (Nbgemmes > 0 && Ks.IsKeyDown(Keys.C) && OKs.IsKeyUp(Keys.C))
                {
                    Nbgemmes--;
                    Gravity = -Gravity;
                    v_y = 0;
                }

            }
            #endregion

            #region Clavier2J1
            else if (TypeClavier == Clavier.Clavier2J1)
            {
                if (Ks.IsKeyDown(Keys.Q) && Ks.IsKeyUp(Keys.D)) //GO LEFT
                {
                    countl++;

                    if (countl == 40)
                        countl = 0;

                    if (countl < 10)
                        SpriteActuel = SpriteLeft1;
                    else if (countl < 20)
                        SpriteActuel = SpriteLeft2;
                    else if (countl < 30)
                        SpriteActuel = SpriteLeft3;
                    else if (countl < 40)
                        SpriteActuel = SpriteLeft4;


                    PosiSprite.X -= Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(-Speed);
                                break;
                            case 2:
                                map.Scroll(-Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpL;
                }

                else if (Ks.IsKeyDown(Keys.D) && Ks.IsKeyUp(Keys.Q)) //GO RIGHT
                {
                    countr++;

                    if (countr == 40)
                        countr = 0;

                    if (countr < 10)
                        SpriteActuel = SpriteRight1;
                    else if (countr < 20)
                        SpriteActuel = SpriteRight2;
                    else if (countr < 30)
                        SpriteActuel = SpriteRight3;
                    else if (countr < 40)
                        SpriteActuel = SpriteRight4;
                    PosiSprite.X += Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(Speed);
                                break;
                            case 1:
                                map.Scroll(Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpR;
                }

                if (Ks.IsKeyDown(Keys.Z) && OnGround) //JUMP
                {
                    InJump = true;
                    sautD = false;
                    if (Gravity > 0)
                        v_y = -7;
                    else v_y = 7;
                }

                if (Ks.IsKeyDown(Keys.Z) && OKs.IsKeyUp(Keys.Z) && !OnGround &&
                    SauvegardesManager.LoadCurrentSave().perso.DoubleSaut && !sautD && JumpCount >= 30)
                {
                    InJump = true;
                    JumpCount = 10;
                    sautD = true;

                    if (Gravity > 0)
                        v_y = -5;
                    else v_y = 5;
                }

                if (Ks.IsKeyDown(Keys.S) && !OnGround && !InJump &&
                    SauvegardesManager.LoadCurrentSave().perso.Propulseur)
                {
                    if (Gravity > 0)
                        Gravity = 2;
                    else Gravity = -2;
                    isV = true;
                }
                else
                {
                    if (Gravity > 0)
                        Gravity = 5;
                    else Gravity = -5;
                    isV = false;
                }

                if (Nbgemmes > 0 && Ks.IsKeyDown(Keys.A) && OKs.IsKeyUp(Keys.A))
                {
                    Nbgemmes--;
                    Gravity = -Gravity;
                    v_y = 0;
                }
            }
            #endregion

            #region Clavier2J2
            else
            {
                if (Ks.IsKeyDown(Keys.Left) && Ks.IsKeyUp(Keys.Right)) //GO LEFT
                {
                    countl++;

                    if (countl == 40)
                        countl = 0;

                    if (countl < 10)
                        SpriteActuel = SpriteLeft1;
                    else if (countl < 20)
                        SpriteActuel = SpriteLeft2;
                    else if (countl < 30)
                        SpriteActuel = SpriteLeft3;
                    else if (countl < 40)
                        SpriteActuel = SpriteLeft4;


                    PosiSprite.X -= Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(-Speed);
                                break;
                            case 2:
                                map.Scroll(-Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpL;
                }

                else if (Ks.IsKeyDown(Keys.Right) && Ks.IsKeyUp(Keys.Left)) //GO RIGHT
                {
                    countr++;

                    if (countr == 40)
                        countr = 0;

                    if (countr < 10)
                        SpriteActuel = SpriteRight1;
                    else if (countr < 20)
                        SpriteActuel = SpriteRight2;
                    else if (countr < 30)
                        SpriteActuel = SpriteRight3;
                    else if (countr < 40)
                        SpriteActuel = SpriteRight4;
                    PosiSprite.X += Speed;

                    int coll = collisionX_with_map(map);

                    if (PosiSpriteEcran.X > 250 && PosiSpriteEcran.X < 450)
                    {
                        switch (coll)
                        {
                            case 0:
                                map.Scroll(Speed);
                                break;
                            case 1:
                                map.Scroll(Speed);
                                break;
                        }
                    }

                    if (InJump)
                        SpriteActuel = SpriteJumpR;
                }

                if (Ks.IsKeyDown(Keys.Up) && OnGround) //JUMP
                {
                    InJump = true;
                    sautD = false;
                    if (Gravity > 0)
                        v_y = -7;
                    else v_y = 7;
                }

                if (Ks.IsKeyDown(Keys.Up) && OKs.IsKeyUp(Keys.Up) && !OnGround &&
                    SauvegardesManager.LoadCurrentSave().perso.DoubleSaut && !sautD && JumpCount >= 30)
                {
                    InJump = true;
                    JumpCount = 10;
                    sautD = true;

                    if (Gravity > 0)
                        v_y = -5;
                    else v_y = 5;
                }

                if (Ks.IsKeyDown(Keys.Down) && !OnGround && !InJump &&
                    SauvegardesManager.LoadCurrentSave().perso.Propulseur)
                {
                    if (Gravity > 0)
                        Gravity = 2;
                    else Gravity = -2;
                    isV = true;
                }
                else
                {
                    if (Gravity > 0)
                        Gravity = 5;
                    else Gravity = -5;
                    isV = false;
                }

                if (Nbgemmes > 0 && Ks.IsKeyDown(Keys.RightControl) && OKs.IsKeyUp(Keys.RightControl))
                {
                    Nbgemmes--;
                    Gravity = -Gravity;
                    v_y = 0;
                }
            }
            #endregion

            OKs = Ks;
                
        }

        private void Animation()
        {
            if (Countanim < 10)
                SpriteActuel = SpriteNormal;
            else if (Countanim < 20)
                SpriteActuel = SpriteNormal2;
            else if (Countanim < 30)
                SpriteActuel = SpriteNormal3;
            else
                Countanim = 0;

            Countanim++;
        }

        private void verif_mort_sortie_map(Map.Map map, GameComponentManager mana)
        {
            if (PosiSprite.Y >= 520 || PosiSprite.Y < 0 - SpriteActuel.Height - 20)
                Mourrir(map, mana);
        }

        private void Invicible()
        {
            if (IsInvicible)
            {
                IsInvicibleCount++;

                if (IsInvicibleCount == 40)
                {
                    IsInvicible = false;
                    IsInvicibleCount = 0;
                }
            }
        }

        private void UpdatePart()
        {
            Random r = new Random();

            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt++;

                int t = 20;
                t += r.Next(-7, 7);

                if (myParticulesTab[i].compt > t)
                {
                    myParticulesTab[i].pos.X = PosiSpriteEcran.X + 11;
                    myParticulesTab[i].pos.Y = PosiSpriteEcran.Y + SpriteActuel.Height - 4;
                    myParticulesTab[i].compt = 0;
                }

                myParticulesTab[i].pos.X += r.Next(-5, 5);
                myParticulesTab[i].pos.Y += 5;

            }
        }

        //======================================PUBLIC ACTIONS===========

        public void KillMonster()
        {
            InJump = true;
            JumpCount = 10;
            sautD = false;

            if (Gravity > 0)
                v_y = -6;
            else v_y = 6;
        }

        public void AddLife()
        {
            Life++;
        }

        public void AddSpeed()
        {
            Speed++;
        }

        public void Perdre(Map.Map map, GameComponentManager mana)
        {
            Life--;

            if (Life == 0)
            {
                Mourrir(map, mana);
            }
            else
            {
                IsInvicible = true;
            }
        }

        public void Mourrir(Map.Map map, GameComponentManager mana)
        {
            if (TypeClavier == Clavier.Clavier1J)
            {
                Gravity = 5;
                PosiSprite = new Vector2(CheckpointX + 16, CheckpointY);
                Life = 3;
                Nbpieces = CheckpointCoins;
                Nbgemmes = CheckpointGemmes;

                mana.delete_all();
                map.Scroll(-map.get_scroll() + CheckpointX - 250);
                map._Posbackground = new Vector2(0, 0);
                map._Posbackground2 = new Vector2(0, 0);
                map.Loading();
            }
            else
            {
                if (Life > 0)
                {
                    Gravity = 5;
                    Life--;
                    Nbpieces = CheckpointCoins;
                    Nbgemmes = CheckpointGemmes;
                    PosiSprite = new Vector2(PosiSprite.X, 0);
                }
                    
            }
        }

        public void add_coins(int numb)
        {
            Nbpieces += numb;
        }

        //======================================PUBLIC GETS==============

        public int get_life()
        {
            return Life;
        }

        public int get_coin()
        {
            return Nbpieces;
        }
    }
}
