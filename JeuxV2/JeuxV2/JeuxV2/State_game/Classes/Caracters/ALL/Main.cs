﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Caracters.ALL
{
    class Main : Player
    {
        public override void Init(Clavier type)
        {
            PosiSprite = new Vector2(80, 344);

            CheckpointX = 80;
            CheckpointY = 344;

            base.Init(type);
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteNormal = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite1");
            SpriteNormal2 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite4");
            SpriteNormal3 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite5");

            SpriteLeft1 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite3");
            SpriteLeft2 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite7");
            SpriteLeft3 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite3");
            SpriteLeft4 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite7");

            SpriteRight1 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite2");
            SpriteRight2 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite6");
            SpriteRight3 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite2");
            SpriteRight4 = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite6");

            SpriteJumpR = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite2");
            SpriteJumpL = content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite3");
            SpriteActuel = SpriteNormal;

            base.LoadContent(content);
        }
    }
}