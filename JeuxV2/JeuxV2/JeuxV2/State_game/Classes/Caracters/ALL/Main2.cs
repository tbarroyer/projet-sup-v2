﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Caracters.ALL
{
    class Main2 : Player
    {
        public override void Init(Clavier type)
        {
            PosiSprite = new Vector2(80, 344);

            CheckpointX = 80;
            CheckpointY = 344;
                        
            base.Init(type);
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteNormal = content.Load<Texture2D>("Sprites/Caracters/Deniz/sprite1");
            SpriteNormal2 = content.Load<Texture2D>("Sprites/Caracters/Deniz/sprite2");
            SpriteNormal3 = content.Load<Texture2D>("Sprites/Caracters/Deniz/sprite1");
            SpriteLeft1 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spritel1");
            SpriteLeft2 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spritel2");
            SpriteLeft3 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spritel3");
            SpriteLeft4 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spritel4");
            SpriteRight1 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spriter1");
            SpriteRight2 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spriter2");
            SpriteRight3 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spriter3");
            SpriteRight4 = content.Load<Texture2D>("Sprites/Caracters/Deniz/spriter4");

            SpriteJumpR = content.Load<Texture2D>("Sprites/Caracters/Deniz/jumpr");
            SpriteJumpL = content.Load<Texture2D>("Sprites/Caracters/Deniz/jumpl");


            SpriteActuel = SpriteNormal;

            base.LoadContent(content);
        }
    }
}