﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Classes.CheckPoint;
using JeuxV2.State_game.Classes.Map;
using JeuxV2.State_game.Classes.Monstres;
using JeuxV2.State_game.Classes.Projectile;
using JeuxV2.State_game.Classes.Ressort;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes
{
    public class GameComponentManager
    {
        private readonly RessortManager _rm;
        private readonly MonstreManager _mm;
        private readonly PowerupManager _pm;
        private readonly CheckpointManager _cm;
        private readonly ProjectileManager _pjm;
        private ContentManager _content;
        private endLevel _end;


        public GameComponentManager(ContentManager content)
        {
            _content = content;
            _rm = new RessortManager(content);
            _mm = new MonstreManager(content);
            _pm = new PowerupManager(content);
            _cm = new CheckpointManager(content);
            _pjm = new ProjectileManager(content);
        }

        public void Update(Map.Map map, Player perso, ContentManager content)
        {
            _pjm.Update(map, perso, this);
            _rm.Update(map, perso);
            _mm.Update(map, perso, this, content);
            _pm.Update(map, perso);
            _cm.Update(map, perso);

            if (_end != null)
                _end.Update(perso, map);
        }

        public void Update(Map.Map map, Player perso1, Player perso2, ContentManager content)
        {
            _pjm.Update(map, perso1, perso2, this);
            _rm.Update(map, perso1, perso2);
            _mm.Update(map, perso1, perso2, this, content);
            _pm.Update(map, perso1, perso2);
            _cm.Update(map, perso1, perso2);

            if (_end != null)
                _end.Update(perso1, perso2, map);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            _rm.Draw(spriteBatch, gameTime, map);
            _mm.Draw(spriteBatch, gameTime, map);
            _pm.Draw(spriteBatch, gameTime, map);
            _cm.Draw(spriteBatch, gameTime, map);
            _pjm.Draw(spriteBatch, gameTime, map);

            if (_end != null)
                _end.Draw(spriteBatch, gameTime, map);
        }

        public void Add(int typeMn, int type, int i, int j)
        {
            switch (typeMn)
            {
                case 1:
                    _mm.AddAMonster(i, j, type);
                    break;

                case 2:
                    _pm.AddAPowerUp(i, j, type);
                    break;

                case 3:
                    _rm.AddARessort(i, j);
                    break;

                case 4:
                    _cm.AddACheckPoint(i, j);
                    break;

                case 5:
                    _end = new endLevel(i,j);
                    _end.LoadContent(_content);
                    break;
            }
        }

        public void delete_all()
        {
            _rm.delete_all();
            _mm.delete_all();
            _pm.delete_all();
            _pjm.delete_all();
            _end = null;
        }

        public void Proj(int x, int y, int jx, int jy, int type, ContentManager content)
        {
            _pjm.AddAProj(x, y, jx, jy, type, content);
        }
    }
}
