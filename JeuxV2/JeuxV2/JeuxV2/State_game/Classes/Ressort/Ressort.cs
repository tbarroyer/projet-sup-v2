﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Ressort
{
    class Ressort : Objet
    {
        int _annim;
        bool _enMarche;

        Texture2D _textureRepo;
        Texture2D _textureMarche;

        public Ressort(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40); //+1: FIXME
        }

        public void Init()
        {
            _annim = 0;
            _enMarche = false;
        }

        public void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Levels/Levels/tiles/tile30");
            _textureRepo = content.Load<Texture2D>("Levels/Levels/tiles/tile30");
            _textureMarche = content.Load<Texture2D>("Levels/Levels/tiles/tile31");
        }

        public void Update(Player perso)
        {
            if (collision_with_other_obj(perso) == 1 && !_enMarche)
            {
                _enMarche = true;
                perso.InJump = true;
                perso.JumpCount = -5;
                perso.v_y = -11;
            }

            SpriteActuel = _textureRepo;
            if (_enMarche)
            {
                SpriteActuel = _textureMarche;
                _annim++;

                if (_annim != 16) return;
                _enMarche = false;
                _annim = 0;
            }
        }
    }
}
