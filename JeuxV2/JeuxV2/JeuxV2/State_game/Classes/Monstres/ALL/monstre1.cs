﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Monstres.ALL
{
    class Monstre1 : Monstre
    {
        public Monstre1(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40 + 1); //+1: FIXME
        }

        public override void Init()
        {
            Speed = new Vector2(2, 2);
            IsDead = false;
            IsFlying = false;
            Gravity = 5;
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/Monsters/Monstre1/Sprite2");
            SpriteArret = content.Load<Texture2D>("Sprites/Monsters/Monstre1/Sprite1");
            SpriteLeft = content.Load<Texture2D>("Sprites/Monsters/Monstre1/Sprite3");
            SpriteRight = content.Load<Texture2D>("Sprites/Monsters/Monstre1/Sprite2");
        }
    }
}