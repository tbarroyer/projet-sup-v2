﻿using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Monstres.ALL
{
    class Monstre4 : Monstre
    {
        private int _count;
        private Texture2D _spriteAction;

        public Monstre4(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40 + 1); //+1: FIXME
            _count = 0;
        }

        public override void Init()
        {
            Speed = new Vector2(0, 0);
            IsDead = false;
            IsFlying = false;
            Gravity = 5;
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/Monsters/Monstre4/Sprite1");
            SpriteArret = content.Load<Texture2D>("Sprites/Monsters/Monstre4/Sprite3");
            SpriteLeft = content.Load<Texture2D>("Sprites/Monsters/Monstre4/Sprite1");
            SpriteRight = content.Load<Texture2D>("Sprites/Monsters/Monstre4/Sprite1");

            _spriteAction = content.Load<Texture2D>("Sprites/Monsters/Monstre4/Sprite2");
        }

        public override void Action(GameComponentManager game, ContentManager content, Player joueur)
        {
            if (_count == 100)
            {
                game.Proj((int)PosiSprite.X, (int)PosiSprite.Y, (int)joueur.PosiSprite.X, (int)joueur.PosiSprite.Y, 1, content);
            }
            else if (_count == 200)
            {
                _count = 0;
            }

            if (_count > 100)
            {
                SpriteActuel = _spriteAction;
            }
            _count++;
        }
    }
}
