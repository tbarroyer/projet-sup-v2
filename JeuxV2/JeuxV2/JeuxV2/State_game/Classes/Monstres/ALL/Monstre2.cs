﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Monstres.ALL
{
    class Monstre2 : Monstre
    {
        public Monstre2(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40 + 1); //+1: FIXME
        }

        public override void Init()
        {
            Speed = new Vector2(4, 0);
            IsDead = false;
            IsFlying = true;
            Gravity = 5;
        }

        public override void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/Monsters/Monstre2/Sprite2");
            SpriteArret = content.Load<Texture2D>("Sprites/Monsters/Monstre2/Sprite1");
            SpriteLeft = content.Load<Texture2D>("Sprites/Monsters/Monstre2/Sprite3");
            SpriteRight = content.Load<Texture2D>("Sprites/Monsters/Monstre2/Sprite2");
        }
    }
}