﻿using System;
using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Monstres
{
    class Monstre : Objet
    {

        protected Texture2D SpriteArret;
        protected Texture2D SpriteLeft;
        protected Texture2D SpriteRight;

        protected Vector2 Speed;

        protected bool IsDead;
        protected bool IsFlying;
        
        //METHODES
        public virtual void Init()
        {

        }

        public virtual void LoadContent(ContentManager content)
        {

        }

        public void Update(Map.Map map, Player perso, GameComponentManager mana, ContentManager content)
        {
            FuturePosition(perso);

            if (collisionX_with_map(map) != 0 && OnGround)
            {
                InJump = true;
                v_y = -5;
                sautD = false;
            }
            collisionY_with_map(map);

            if (!IsDead)
            {
                switch (collision_with_other_obj(perso))
                {
                    case 1:
                        perso.KillMonster();
                        Mort();
                        break;

                    case 2:
                        if (!perso.IsInvicible)
                            perso.Perdre(map, mana);
                        break;
                }
            }

            if (!IsDead)
                Action(mana, content, perso);
        }

        public void Update(Map.Map map, Player perso1, Player perso2, GameComponentManager mana, ContentManager content)
        {
            if (Math.Abs(perso2.PosiSprite.X - PosiSprite.X) <= Math.Abs(perso1.PosiSprite.X - PosiSprite.X))
                FuturePosition(perso2);
            else FuturePosition(perso1);

            if (collisionX_with_map(map) != 0 && OnGround)
            {
                InJump = true;
                v_y = -5;
                sautD = false;
            }
            collisionY_with_map(map);

            if (!IsDead)
            {
                switch (collision_with_other_obj(perso1))
                {
                    case 1:
                        perso1.KillMonster();
                        Mort();
                        break;

                    case 2:
                        if (!perso1.IsInvicible)
                            perso1.Perdre(map, mana);
                        break;
                }

                switch (collision_with_other_obj(perso2))
                {
                    case 1:
                        perso2.KillMonster();
                        Mort();
                        break;

                    case 2:
                        if (!perso2.IsInvicible)
                            perso2.Perdre(map, mana);
                        break;
                }
            }

            if (!IsDead)
            {
                if (Math.Abs(perso2.PosiSprite.X - PosiSprite.X) <= Math.Abs(perso1.PosiSprite.X - PosiSprite.X))
                    Action(mana, content, perso2);
                else Action(mana, content, perso1);
            }
                
        }

        private void FuturePosition(Player perso)
        {
            if (!IsDead)
            {
                if (perso.PosiSprite.X < PosiSprite.X - 3 && (PosiSprite.X - perso.PosiSprite.X) < 700)
                {
                    SpriteActuel = SpriteLeft;
                    PosiSprite.X -= Speed.X;
                }
                else if (perso.PosiSprite.X > PosiSprite.X + 3 && (perso.PosiSprite.X - PosiSprite.X) < 700)
                {
                    SpriteActuel = SpriteRight;
                    PosiSprite.X += Speed.X;
                }
            }

            if (!IsFlying)
                apply_gravity();
                
        }
        
        private void Mort()
        {
            SpriteActuel = SpriteArret;
            IsDead = true;
            IsFlying = false;
        }

        public virtual void Action(GameComponentManager game, ContentManager content, Player joueur)
        {

        }

    }
}
