﻿using System.Collections.Generic;
using System.Linq;
using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Classes.Monstres.ALL;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Monstres
{
    public class MonstreManager
    {
        private readonly List<Monstre> _m1 = new List<Monstre>();
        private int _numberMonsterDraw;
        private readonly ContentManager _content;

        public MonstreManager(ContentManager content)
        {
            _content = content;
        }



        public void Update(Map.Map map, Player perso, GameComponentManager mana, ContentManager content)
        {
            for (int i = 0; i < (int)_m1.LongCount(); i++)
            {
                _m1[i].Update(map, perso, mana, content);
            }
        }

        public void Update(Map.Map map, Player perso1, Player perso2, GameComponentManager mana, ContentManager content)
        {
            for (int i = 0; i < (int)_m1.LongCount(); i++)
            {
                _m1[i].Update(map, perso1, perso2, mana, content);
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            for (int i = 0; i < (int)_m1.LongCount(); i++)
            {
                _m1[i].Draw(spriteBatch, gameTime, map); //Drawing the monster
            }
        }

        public void AddAMonster(int i, int j, int type)
        {
            switch (type)
            {
                case 1:
                    _m1.Add(new Monstre1(i, j));
                    break;
                case 2:
                    _m1.Add(new Monstre2(i, j));
                    break;
                case 3:
                    _m1.Add(new Monstre3(i, j));
                    break;
                case 4:
                    _m1.Add(new Monstre4(i, j));
                    break;
            }
            
            _m1[_numberMonsterDraw].Init();
            _m1[_numberMonsterDraw].LoadContent(_content);
            
            _numberMonsterDraw++;
        }

        public void delete_all()
        {
            _m1.Clear();
            _numberMonsterDraw = 0;
        }

    }
}