﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_game.Classes.Map
{
    class endLevel : Objet
    {
        public endLevel(int posx, int posy)
        {
            PosiSprite = new Vector2(posx * 40, posy * 40); //+1: FIXME
        }

        public void LoadContent(ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/end");
        }

        public void Update(Player perso, Map m)
        {
            if (collision_with_other_obj(perso) > 0)
            {
                Sauvegardes.SauvegardesManager.SaveGameData data = Sauvegardes.SauvegardesManager.LoadCurrentSave();
                data.nmPièces += perso.Nbpieces;
                data.UnlockLvl[m.IdLevel + 1] = true;
                Sauvegardes.SauvegardesManager.SaveCurrentSave(data);
                STATE_MANAGER.StateManager.goto_state("selection", Keyboard.GetState());
            }
        }

        public void Update(Player perso1, Player perso2, Map m)
        {
            if (collision_with_other_obj(perso1) > 0 || collision_with_other_obj(perso2) > 0)
            {
                Sauvegardes.SauvegardesManager.SaveGameData data = Sauvegardes.SauvegardesManager.LoadCurrentSave();
                data.nmPièces += perso1.Nbpieces + perso2.Nbpieces;
                data.UnlockLvl[m.IdLevel + 1] = true;
                Sauvegardes.SauvegardesManager.SaveCurrentSave(data);
                STATE_MANAGER.StateManager.goto_state("selection", Keyboard.GetState());
            }
        }
    }
}
