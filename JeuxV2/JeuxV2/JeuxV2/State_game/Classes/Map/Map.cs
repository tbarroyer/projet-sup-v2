﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Map
{
    public class Map
    {
        //ENUM
        public enum Monde
        {
            Mac,
            Windows,
            Linux
        }

        //STRUCTURES
        private struct Tab //Lvl informations
        {
            public struct Sizem
            {
                public int X; //size of x in map
                public int Y; //size of y in map
            }

            public Sizem Size;

            public bool[] Collision;

            public int[,] Pattern; //The patern of the map: 1 int = 1 sprite

            public Monde TypeMonde;
        }

        //MEMBERS
        private Texture2D[] _tile;

        private Tab _lvl;

        private int idLevel;
        public int IdLevel
        {
            get { return idLevel; }
        }

        private int _startX;

        private Texture2D _background;
        private Texture2D _background2;
        private Vector2 _posbackground2;
        public Vector2 _Posbackground2
        {
            set { _posbackground2 = value; }
        }

        private Vector2 _posbackground;
        public Vector2 _Posbackground
        {
            set { _posbackground = value; }
        }

        private readonly string _file;

        //METHODES
        public Map(string file, int IDfile)
        {
            _file = file;
            idLevel = IDfile;
        }

        public void Init()
        {
            _startX = 0;
            _posbackground = new Vector2(0, 0);
            _posbackground2 = new Vector2(0, 0);
            Loading();

        }

        public void LoadContent(ContentManager content)
        {
            _tile = new Texture2D[16];

            string type = "";

            switch (_lvl.TypeMonde)
            {
                case Monde.Windows:
                    type = "tiles";
                    break;
                case Monde.Mac:
                    type = "tilesneige";
                    break;
                case Monde.Linux:
                    type = "tilesdesert";
                    break;
            }

            _tile[1] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile1");
            _tile[2] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile2");
            _tile[3] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile3");
            _tile[4] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile4");
            _tile[5] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile5");
            _tile[6] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile6");
            _tile[7] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile7");
            _tile[8] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile8");
            _tile[9] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile9");
            _tile[10] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile10");
            _tile[11] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile11");
            _tile[12] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile12");
            _tile[13] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile13");
            _tile[14] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile14");
            _tile[15] = content.Load<Texture2D>("Levels/Levels/" + type + "/tile15");

            _background = content.Load<Texture2D>("Levels/back2");
            _background2 = content.Load<Texture2D>("Levels/back3");
        }

        public void Update()
        {
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, GameComponentManager mana)
        {
            
            spriteBatch.Begin();

            spriteBatch.Draw(_background, _posbackground, Color.White);
           spriteBatch.Draw(_background, _posbackground + new Vector2(0 - 4 *_background.Width, 0), Color.White);
            spriteBatch.Draw(_background, _posbackground + new Vector2(0 - 3 * _background.Width, 0), Color.White);
           spriteBatch.Draw(_background, _posbackground + new Vector2(0 - 2 * _background.Width, 0), Color.White);
           spriteBatch.Draw(_background, _posbackground + new Vector2(0 - _background.Width, 0), Color.White);
            spriteBatch.Draw(_background, _posbackground + new Vector2(_background.Width, 0), Color.White);
            spriteBatch.Draw(_background, _posbackground + new Vector2(_background.Width*2, 0), Color.White);
            spriteBatch.Draw(_background, _posbackground + new Vector2(_background.Width*3, 0), Color.White);
           spriteBatch.Draw(_background, _posbackground + new Vector2(_background.Width*4, 0), Color.White);

            spriteBatch.Draw(_background2, _posbackground2, Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(0 - 4 * _background.Width, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(0 - 3 * _background.Width, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(0 - 2 * _background.Width, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(0 - _background.Width, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(_background.Width, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(_background.Width * 2, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(_background.Width * 3, 0), Color.White);
            spriteBatch.Draw(_background2, _posbackground2 + new Vector2(_background.Width * 4, 0), Color.White);

            var X = -(_startX % 40);
            var Y = 0;

            int i;
            for (i = 0; i < _lvl.Size.Y; i++) //Loop drawing all tiles
            {
                X = -(_startX % 40);

                int j;
                for (j = _startX / 40; j < 20 + 1 + _startX / 40; j++)
                {
                    int n = _lvl.Pattern[i, j];
                    if (n != 0)
                    {
                        if (n < 21)
                        {
                            spriteBatch.Draw(_tile[n], new Vector2(X, Y), Color.White);
                        }
                        else
                        {
                            switch (n)
                            {
                                #region ajouts éléments
                                case 21:
                                    mana.Add(1, 1, j, i);
                                    break;
                                case 22:
                                    mana.Add(1, 2, j, i);
                                    break;
                                case 23:
                                    mana.Add(1, 3, j, i);
                                    break;
                                case 24:
                                    mana.Add(1, 4, j, i);
                                    break;
                                case 30:
                                    mana.Add(3, 1, j, i);
                                    break;
                                case 50:
                                    mana.Add(2, 1, j, i);
                                    break;
                                case 51:
                                    mana.Add(2, 2, j, i);
                                    break;
                                case 52:
                                    mana.Add(2, 3, j, i);
                                    break;
                                case 53:
                                    mana.Add(2, 4, j, i);
                                    break;
                                case 98:
                                    mana.Add(5, 1, j, i);
                                    break;
                                case 99:
                                    mana.Add(4, 1, j, i);
                                    break;
                                #endregion

                            }
                        
                            _lvl.Pattern[i, j] = 0;
                        }
                    }

                    X += 40;
                }

                Y += 40;
            }
            spriteBatch.End();
        }

        /// <summary>
        /// Getting infos from the level file
        /// </summary>
        public void Loading()
        {
            var streamReader = new StreamReader(Path.GetFullPath("Content/Levels/Levels/" + _file));

            int type = int.Parse(streamReader.ReadLine());

            switch (type)
            {
                case 1:
                    _lvl.TypeMonde = Monde.Windows;
                    break;
                case 2:
                    _lvl.TypeMonde = Monde.Mac;
                    break;
                case 3:
                    _lvl.TypeMonde = Monde.Linux;
                    break;
            }
            

            streamReader.ReadLine();

// ReSharper disable once AssignNullToNotNullAttribute
            _lvl.Size.Y = int.Parse(streamReader.ReadLine());
// ReSharper disable once AssignNullToNotNullAttribute
            _lvl.Size.X = int.Parse(streamReader.ReadLine());

            streamReader.ReadLine();

            _lvl.Pattern = new int[_lvl.Size.Y, _lvl.Size.X];

// ReSharper disable once AssignNullToNotNullAttribute
            int nColl = int.Parse(streamReader.ReadLine());

            _lvl.Collision = new bool[nColl];

            streamReader.ReadLine();

            for (int i = 0; i < nColl; i++) //Get collision informations
            {
                if (Char.GetNumericValue((char)streamReader.Read()) == 0)
                    _lvl.Collision[i] = false;
                else _lvl.Collision[i] = true;
                streamReader.ReadLine();
            }

            streamReader.ReadLine();

            for (var i = 0; i < _lvl.Size.Y; i++) //Loop loading all tiles
            {
                for (var j = 0; j < _lvl.Size.X; j++)
                {
                    _lvl.Pattern[i, j] = (int)(Char.GetNumericValue((char)streamReader.Read())) * 10;
                    _lvl.Pattern[i, j] += (int)(Char.GetNumericValue((char)streamReader.Read()));

                    streamReader.Read();
                }
                streamReader.Read();
            }

            streamReader.Close();
        }


        //SETTING METHODES
        public void Scroll(int x)
        {
            if (_startX >= 0 && _startX <= (_lvl.Size.X - 21) * 40)
            {
                _startX += x;

                _posbackground.X -= x / 3;

                _posbackground2.X -= x / 2;
            }

            if (_startX < 0)
                _startX = 0;
            else if (_startX > (_lvl.Size.X - 21) * 40)
            {
                _startX = (_lvl.Size.X - 21) * 40;
            }
        }


        //GETTING METHODES

        /// <summary>
        /// Get values of block in X Y position in the map
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <returns></returns>
        public int get_lvl_pattern(int x, int y)
        {
            if (0 <= x && x < _lvl.Size.X && 0 <= y && y < _lvl.Size.Y)
                return _lvl.Pattern[y, x];
            return 0;
        }

        /// <summary>
        /// Getting the collision of a tile (0, no collision 1, collision)
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns></returns>
        public bool get_lvl_collision(int x, int y)
        {
            return _lvl.Collision[get_lvl_pattern(x, y)];
        }

        /// <summary>
        /// Getting the scrolling
        /// </summary>
        /// <returns></returns>
        public int get_scroll()
        {
            return _startX;
        }

        public Monde GetMonde()
        {
            return _lvl.TypeMonde;
        }
    }
}