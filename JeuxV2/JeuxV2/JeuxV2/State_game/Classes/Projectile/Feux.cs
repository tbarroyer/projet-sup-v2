﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Projectile
{
    class Feux : Projectile
    {
        //METHODES
        public Feux(int x, int y, int jx, int jy, ContentManager content)
        {
            SpriteActuel = content.Load<Texture2D>("Sprites/shuriken");
            PosiSprite = new Vector2(x + 20, y -20);

            Direction = new Vector2(0, 0);

            Direction.X = jx - x;
            Direction.Y = jy - y + 30;

            Speed = Direction / 100;

            if (Direction.X < 0)
                Droite = false;
            else
                Droite = true;
        }

        public override void Init()
        {

        }

        public override void LoadContent(ContentManager content)
        {

        }
    }
}