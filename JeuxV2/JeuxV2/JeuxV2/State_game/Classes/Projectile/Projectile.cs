﻿using JeuxV2.State_game.Classes.Caracters;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Projectile
{
    internal class Projectile : Objet
    {
        protected Vector2 Direction;
        protected Vector2 Speed;

        protected bool Droite;

        //METHODES

        public virtual void Init()
        {
        }

        public virtual void LoadContent(ContentManager content)
        {
        }

        public bool Update(Map.Map map, Player joueur, GameComponentManager gcm)
        {
            PosiSprite.X += Speed.X;
            PosiSprite.Y += Speed.Y;

            if (collision_with_other_obj(joueur) != 0 && !joueur.IsInvicible)
            {
                return true;
            }
            return false;
        }

        public int Update(Map.Map map, Player joueur1, Player joueur2, GameComponentManager gcm)
        {
            int test = 0;

            PosiSprite.X += Speed.X;
            PosiSprite.Y += Speed.Y;

            if (collision_with_other_obj(joueur1) != 0 && !joueur1.IsInvicible)
            {
                test = 1;

                if (collision_with_other_obj(joueur2) != 0 && !joueur2.IsInvicible)
                {
                    return 3;
                }
            }

            if (collision_with_other_obj(joueur2) != 0 && !joueur2.IsInvicible)
            {
                test = 2;
            }

            return test;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            set_posi_sprite_ecran(map);

            spriteBatch.Begin();

            if (Droite)
                spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, Color.White);
            else
                spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, null, Color.White, 0, new Vector2(0), 1,
                    SpriteEffects.FlipHorizontally, 0);
            spriteBatch.End();
        }
    }
}