﻿using System.Collections.Generic;
using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Classes.Projectile
{
    class ProjectileManager
    {
        private readonly List<Projectile> _p1 = new List<Projectile>();
        public ContentManager Content { get; set; }

        public ProjectileManager(ContentManager content)
        {
            Content = content;
        }

        public void Update(Map.Map map, Player joueur, GameComponentManager gcm)
        {
            bool test = false;

            foreach (var l in _p1)
            {
                if (l.Update(map, joueur, gcm))
                    test = true;
            }

            if (test)
                joueur.Perdre(map, gcm);
        }

        public void Update(Map.Map map, Player joueur1, Player joueur2, GameComponentManager gcm)
        {
            bool test1 = false;
            bool test2 = false;

            int test = 0;

            foreach (var l in _p1)
            {
                test = l.Update(map, joueur1, joueur2, gcm);

                switch (test)
                {
                    case 0:
                        break;
                    case 1:
                        test1 = true;
                        break;
                    case 2 :
                        test2 = true;
                        break;
                    case 3 :
                        test1 = true;
                        test2 = true;
                        break;
                }
            }

            if (test1)
                joueur1.Perdre(map, gcm);

            if (test2)
                joueur2.Perdre(map, gcm);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map.Map map)
        {
            foreach (var l in _p1)
            {
                l.Draw(spriteBatch, gameTime, map);
            }
        }

        public void AddAProj(int x, int y, int jx, int jy, int type, ContentManager content)
        {
            switch (type)
            {
                case 1: _p1.Add(new Feux(x, y, jx, jy, content));
                    break;
            }
        }

        public void delete_all()
        {
            _p1.Clear();
        }
    }
}
