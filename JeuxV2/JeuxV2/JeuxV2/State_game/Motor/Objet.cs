﻿using System.Runtime.Serialization.Formatters;
using JeuxV2.State_game.Classes.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.Motor
{
    public class Objet
    {
        public bool InJump;
        public Texture2D SpriteActuel;
        public int JumpCount;

        public int Gravity;
        public bool OnGround;

        public Vector2 PosiSprite;
        public Vector2 PosiSpriteEcran;

        public int v_y;

        protected bool sautD;

        //===============================PHYSICS================================

        public void apply_gravity()
        {
            JumpCount++;

            if (!InJump)
            {
                PosiSprite.Y += Gravity;
            }
            else
            {
                PosiSprite.Y += v_y;

                if (JumpCount%3 == 0)
                {
                    if (Gravity > 0)
                        v_y++;
                    else v_y--;
                }
                    

                if (v_y == 0)
                    InJump = false;
            }
        }

        public void collisionY_with_map(Map map)
        {
            if (Gravity > 0)
            {
                bool t1 = map.get_lvl_collision(((int) PosiSprite.X + 5)/40,
                    ((int) PosiSprite.Y + SpriteActuel.Height)/40);
                bool t2 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width - 5)/40,
                    ((int) PosiSprite.Y + SpriteActuel.Height)/40);

                if ((t1 || t2) && !InJump)
                {
                    int ecart = ((int) PosiSprite.Y + SpriteActuel.Height)%40;
                    PosiSprite.Y = PosiSprite.Y - ecart;
                    OnGround = true;
                    sautD = false;
                }
                else OnGround = false;

                t1 = map.get_lvl_collision(((int) PosiSprite.X + 5)/40, (int) PosiSprite.Y/40);
                t2 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width - 5)/40, (int) PosiSprite.Y/40);

                if (t1 || t2)
                    InJump = false;
            }

            else
            {
                bool t1 = map.get_lvl_collision(((int) PosiSprite.X + 5)/40,
                    ((int) PosiSprite.Y + SpriteActuel.Height)/40);
                bool t2 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width - 5)/40,
                    ((int) PosiSprite.Y + SpriteActuel.Height)/40);

                if (t1 || t2)
                    InJump = false;

                t1 = map.get_lvl_collision(((int) PosiSprite.X + 5)/40, (int) PosiSprite.Y/40);
                t2 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width - 5)/40, (int) PosiSprite.Y/40);

                if ((t1 || t2) && !InJump)
                {
                    int ecart = 40 - ((int) PosiSprite.Y)%40;
                    PosiSprite.Y = PosiSprite.Y + ecart;
                    OnGround = true;
                    sautD = false;
                }
                else OnGround = false;
            }
        }

        public int collisionX_with_map(Map map)
        {
            bool l1 = map.get_lvl_collision(((int) PosiSprite.X)/40, (int) PosiSprite.Y/40);
            bool l2 = map.get_lvl_collision(((int) PosiSprite.X)/40,
                ((int) PosiSprite.Y + SpriteActuel.Height - 6)/40);

            bool r1 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width)/40, (int) PosiSprite.Y/40);
            bool r2 = map.get_lvl_collision(((int) PosiSprite.X + SpriteActuel.Width)/40,
                ((int) PosiSprite.Y + SpriteActuel.Height - 6)/40);

            if (r1 || r2) // DROITE
            {
                PosiSprite.X = ((int) PosiSprite.X + SpriteActuel.Width)/40*40 - SpriteActuel.Width;
                return 2;
            }

            if (l1 || l2) // DROITE
            {
                PosiSprite.X = (((int) PosiSprite.X)/40 + 1)*40;
                return 1;
            }

            return 0;
        }

        public int collision_with_other_obj(Objet obj)
        {
            if ((obj.PosiSprite.X >= PosiSprite.X + SpriteActuel.Width)
                || (obj.PosiSprite.X + obj.SpriteActuel.Width <= PosiSprite.X)
                || (obj.PosiSprite.Y >= PosiSprite.Y + SpriteActuel.Height)
                || (obj.PosiSprite.Y + obj.SpriteActuel.Height <= PosiSprite.Y)
                )
            {
                return 0;
            }

            if (obj.PosiSprite.Y + obj.SpriteActuel.Height <= PosiSprite.Y + 5)
            {
                return 1;
            }
            return 2;
        }

        //==============================IA======================================

        //===============================XNA====================================

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Map map)
        {
            set_posi_sprite_ecran(map);

            spriteBatch.Begin();

            if (Gravity >= 0)
                spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, Color.White);
            else
                spriteBatch.Draw(SpriteActuel, PosiSpriteEcran, null, Color.White, 0, new Vector2(0), 1,
                    SpriteEffects.FlipVertically, 0);
            spriteBatch.End();
        }

        public void set_posi_sprite_ecran(Map map)
        {
            PosiSpriteEcran.Y = PosiSprite.Y;
            PosiSpriteEcran.X = PosiSprite.X - map.get_scroll();
        }
    }
}