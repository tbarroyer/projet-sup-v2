﻿using JeuxV2.State_game.Classes;
using JeuxV2.State_game.Classes.Caracters.ALL;
using JeuxV2.State_game.Classes.Map;
using JeuxV2.State_game.HUD;
using JeuxV2.STATE_MANAGER;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using JeuxV2.State_game.Classes.Caracters;

namespace JeuxV2.State_game
{
    class Gamep : State
    {
        //VARIABLES
        private Map _map;   //The map
        private Player _main; //The player
        private Hud _hud;

        private GameComponentManager _mana;

        private KeyboardState _oldkeyboardState;

        private Snow_Motor.SnowMotor _sm;

        private bool isPause;

        //FONCTIONS
        public Gamep(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            :            base(device, "gamep", content, spriteBatch)
        {
        }

        public override bool Init(KeyboardState oldkeyboardState, string level, int IDfile)
        {
            _oldkeyboardState = oldkeyboardState;

            SpriteBatch = new SpriteBatch(Device);

            _map = new Map(level, IDfile);
            _map.Init();
            _map.LoadContent(Content);

            switch (Sauvegardes.SauvegardesManager.LoadCurrentSave().perso.TypeJoueur)
            {
                case 1 :
                    _main = new Main();
                    break;

                case 2 :
                    _main = new Main2();
                    break;

            }
            _main.Init(Player.Clavier.Clavier1J);
            _main.LoadContent(Content);

            _mana = new GameComponentManager(Content);

            _hud = new Hud();
            _hud.LoadContent(Content);

            _sm = new Snow_Motor.SnowMotor();
            _sm.LoadContent(Content);
            _sm.Initialize();

            isPause = false;

            return base.Init(_oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            _map.Draw(SpriteBatch, gameTime, _mana);

            _mana.Draw(SpriteBatch, gameTime, _map);

            _main.Draw(SpriteBatch, gameTime, _map);

            _hud.Draw(SpriteBatch, gameTime, _main, Hud.Position.gauche);

            if (_map.GetMonde() == Map.Monde.Mac)
                _sm.DrawSnow(SpriteBatch, Content, _map, gameTime);

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (!isPause)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.R))
                {
                    StateManager.goto_state("selection", Keyboard.GetState()); //Back to main menu
                }

                _map.Update();

                _main.Update(_map, _mana);

                _mana.Update(_map, _main, Content);

                _hud.Update();

            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape) && _oldkeyboardState.IsKeyUp(Keys.Escape))
                isPause = !isPause;

            _oldkeyboardState = Keyboard.GetState();

            base.Update(gameTime, graphics);
        }

    }
}