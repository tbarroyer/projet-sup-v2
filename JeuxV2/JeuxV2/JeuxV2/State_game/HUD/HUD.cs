﻿using JeuxV2.State_game.Classes.Caracters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_game.HUD
{
    class Hud
    {

        //VARIABLES
        private SpriteFont _font;
        private Texture2D _heart;
        private Texture2D _coin;
        private Texture2D _gemme;

        public enum Position
        {
            gauche,
            droite
        }

        //METHODES

        public void Init()
        {

        }

        public void LoadContent(ContentManager content)
        {
            _heart = content.Load<Texture2D>("Sprites/heart");
            _font = content.Load<SpriteFont>("Font/SpriteFont3");
            _coin = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin1");
            _gemme = content.Load<Texture2D>("Sprites/Power Up/Power2");

        }

        public void Update()
        {

        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Player p1, Position position)
        {
            spriteBatch.Begin();

            if (position == Position.gauche)
            {
                spriteBatch.Draw(_heart, new Vector2(5, 5), Color.White);
                spriteBatch.Draw(_coin, new Vector2(5, 50), Color.White);
                spriteBatch.Draw(_gemme, new Vector2(5, 100), Color.White);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.get_life()), new Vector2(40, 8), Color.Black);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.get_coin()), new Vector2(40, 58), Color.Black);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.Nbgemmes), new Vector2(40, 108), Color.Black);
            }
            else
            {
                spriteBatch.Draw(_heart, new Vector2(800 - 120, 5), Color.White);
                spriteBatch.Draw(_coin, new Vector2(800 - 120, 50), Color.White);
                spriteBatch.Draw(_gemme, new Vector2(800 - 120, 100), Color.White);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.get_life()), new Vector2(800 - 80, 8), Color.Black);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.get_coin()), new Vector2(800 - 80, 58), Color.Black);
                spriteBatch.DrawString(_font, " X " + System.Convert.ToString(p1.Nbgemmes), new Vector2(800 - 80, 108), Color.Black);
            }
            
            spriteBatch.End();
        }
    }
}
