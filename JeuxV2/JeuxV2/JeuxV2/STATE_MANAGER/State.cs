﻿using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using JeuxV2.State_menu;
namespace JeuxV2.STATE_MANAGER
{
    public class State
    {
        protected GraphicsDevice Device = null;
        protected ContentManager Content = null;
        protected SpriteBatch SpriteBatch = null;
        protected Game Game = null;
        //type of State for the state manager : null fort default, or level, or menu//
        protected string type = null;

        /// <summary>
        /// State Constructor
        /// </summary>
        /// <param name="name">Must be unique since when you use StateManager is per name</param>

        public State(GraphicsDevice device, string name, ContentManager content, SpriteBatch spriteBatch)
        {
            Name = name;
            Device = device;
            Content = content;
            SpriteBatch = spriteBatch;
   
        }
        /* public bool IsMenu
         {
             get;
             set;
         }
 */
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Name
        {
            get;
            set;
        }

  
        /// <summary>
        /// Virtual Function that's called when entering a State
        /// override it and add your own initialization code
        /// </summary>
        /// <returns></returns>
        public virtual bool Init(KeyboardState oldkeyboardState)
        {
            return true;
        }

        public virtual bool Init(KeyboardState oldkeyboardState, string level, int IDlvl)
        {
            return true;
        }
        public virtual bool Init(KeyboardState oldkeyboardState, VaisseauBase vaisseau,Laser_Manager  LM  )
        {
            return true;
        }


        /// <summary>
        /// Virtual Function that's called when exiting a State
        /// override it and add your own shutdown code
        /// </summary>
        /// <returns></returns>
        public virtual void Shutdown()
        {
        }

        /// <summary>
        /// Override it to have access to elapsed time
        /// </summary>
        public virtual void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {

        }
        public virtual void Update(GameTime gameTime, GraphicsDeviceManager graphics,Game game)
        {

        }
        public virtual void Draw(GameTime gameTime)
        {

        }

    }

}
