﻿using System.Collections.Generic;
using System.Linq;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.STATE_MANAGER
{
    /// <summary>
    /// Screen Manager
    /// Keeps a list of available state
    /// so you can switch between them, 
    /// ie. jumping from the start screen to the game screen 
    /// </summary>
    public static class StateManager
    {
        // Protected Members
        static private readonly List<State> States = new List<State>();
        static private bool _started;
        static private State _previous;
        // Public Members
        static public State ActiveState = null;

        /// <summary>
        /// Add new State
        /// </summary>
        static public void add_state(State state)
        {
            if (States.Any(scr => scr.Name == state.Name))
            {
                return;
            }
            States.Add(state);
        }

        static public int get_state_number()
        {
            return States.Count;
        }

        static public State get_state(int idx)
        {
            return States[idx];
        }

        /// <summary>
        /// Go to screen
        /// </summary>
        /// <param name="name">Screen name</param>
        /// <param name="oldkeyboardState"></param>
        static public void goto_state(string name, KeyboardState oldkeyboardState)
        {
            foreach (var state in States)
            {
                if (state.Name == name)
                {
                    // Shutsdown Previous Screen           
                    _previous = ActiveState;
                    if (ActiveState != null)
                    {
                        ActiveState.Shutdown();
                    }
                    // Inits New Screen
                    ActiveState = state;
                    if (_started) ActiveState.Init(oldkeyboardState);
                    return;
                }
            }
        }
        static public void goto_state(string name, KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
        {
            foreach (var state in States)
            {
                if (state.Name == name)
                {
                    // Shutsdown Previous Screen           
                    _previous = ActiveState;
                    if (ActiveState != null)
                    {
                        ActiveState.Shutdown();
                    }
                    // Inits New Screen
                    ActiveState = state;
                    if (_started) ActiveState.Init(oldkeyboardState,vaisseau,LM);
                    return;
                }
            }
        }

        static public void goto_state(string name, KeyboardState oldkeyboardState, string level, int IDfile)
        {
            foreach (var state in States)
            {
                if (state.Name == name)
                {
                    // Shutsdown Previous Screen           
                    _previous = ActiveState;
                    if (ActiveState != null)
                    {
                        ActiveState.Shutdown();
                    }
                    // Inits New Screen
                    ActiveState = state;
                    if (_started) ActiveState.Init(oldkeyboardState, level, IDfile);
                    return;
                }
            }
        }


        /// <summary>
        /// Init Screen manager
        /// Only at this point is screen manager going to init the selected screen
        /// </summary>
        static public void Init(KeyboardState oldkeyboardState)
        {
            _started = true;
            if (ActiveState != null)
            {
                ActiveState.Init(oldkeyboardState);
            }
        }
        static public void Init(KeyboardState oldkeyboardState, VaisseauBase vaisseau, Laser_Manager LM)
        {
            _started = true;
            if (ActiveState != null)
            {
                ActiveState.Init(oldkeyboardState,vaisseau, LM);
            }
        }
        static public void Init(KeyboardState oldkeyboardState, string level, int IDlvl)
        {
            _started = true;
            if (ActiveState != null)
            {
                ActiveState.Init(oldkeyboardState, level, IDlvl);
            }
        }
     
    
        /// <summary>
        /// Falls back to previous selected state if any
        /// </summary>
        static public void go_back(params object[]array)
        {
            if (_previous == null) return;

                if (_previous.Type==null)
                   

            goto_state(_previous.Name, (KeyboardState) array[0]);
            else if(_previous.Type=="menu")

                goto_state(_previous.Name,(KeyboardState) array[0],(VaisseauBase) array[1],(Laser_Manager) array[2]);
        
        }


        /// <summary>
        /// Updates Active State
        /// </summary>
        static public void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (_started == false) return;
            if (ActiveState != null)
            {
                ActiveState.Update(gameTime, graphics);
            }
        }

        static public void Draw(GameTime gameTime)
        {
            if (_started == false) return;
            if (ActiveState != null)
            {
                ActiveState.Draw(gameTime);
            }
        }
    }
}
