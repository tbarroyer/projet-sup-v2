﻿using System;
using JeuxV2.State_game.Classes.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.Snow_Motor
{
    class SnowMotor
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public void Initialize()
        {
            Width = 1600;
            Height = 600;
            _isSnowing = true;
            _snow = new Snow[600];
            _random = new Random();
        }

        private Snow[] _snow;  //You can increase/decrease this number depending on the density you want
        private bool _isSnowing;           //I only set this to true (so far) in the main menu
        private Texture2D _snowTexture;           //Our texture for each snow particle. I'll attach mine, though it's very simple
        private Point _quitPoint;                       //The point at which we need to recycle the snow particle
        private Point _startPoint;                     //The point where the snow particle gets recycled to
        private Random _random;                    //Will be used for generating new random numbers

        public void LoadContent(ContentManager content)
        {
            _snowTexture = content.Load<Texture2D>("Sprites/snow");

            //Set the snow's quit point. It's the bottom of the screen plus the texture's height so it looks like the snow goes completely off screen
            _quitPoint = new Point(0,
                600 + _snowTexture.Height);

            //Set the snow's start point. It's the top of the screen minus the texture's height so it looks like it comes from somewhere, rather than appearing
            _startPoint = new Point(0,
                0 - _snowTexture.Height);
        }

        public void DrawSnow(SpriteBatch sb, ContentManager content, Map map, GameTime gameTime)
        {
            //If it's not supposed to be snowing, exit
            if (!_isSnowing)
                return;

            //This will be used as the index within our snow array
            int i;

            //NOTE: The following conditional is not exactly the best "initializer."
            //If snow has not been initialized
            if (_snow[0] == null)
            {

                //For every snow particle within our snow array,
                for (i = 0; i < _snow.Length; i++)
                    //Give it a new, random x and y. This will give the illusion that it was already snowing
                    //and won't cluster the particles
                    _snow[i] = new Snow(new Vector2(
                        (map.get_scroll() + _random.Next(0, (Width - _snowTexture.Width))),
                        (_random.Next(0, (Height)))), content);
            }

            //Make the random a new random (again, if just starting)
            _random = new Random();

            //Go back to the beginning of the snow array
            i = 0;

            //Begin displaying the snow
            foreach (var snowPnt in _snow)
            {
                //Get the exact rectangle for the snow particle
                var snowParticle = new Rectangle(
                    (int)snowPnt.PosiSprite.X, (int)snowPnt.PosiSprite.Y, _snowTexture.Width, _snowTexture.Height);

                //Draw the snow particle (change white if you want any kind of tinting)
                snowPnt.Draw(sb, gameTime, map);
                //Make the current particle go down, but randomize it for a staggering snow
                _snow[i].PosiSprite.Y += _random.Next(0, 5);
                _snow[i].PosiSprite.X -= 5;

                //Make sure the point's location is not below quit point's
                if (_snow[i].PosiSprite.Y >= _quitPoint.Y)
                    //If it is, give it a random X value, and the starting point variable's Y value
                    _snow[i] = new Snow(new Vector2(
                        (_random.Next(map.get_scroll() + 0, map.get_scroll() + (Width*2 - _snowTexture.Width))),
                        _startPoint.Y), content);

                //Increment our position in the array ("go to the next snow particle")
                i++;
            }
        }
    }
}
