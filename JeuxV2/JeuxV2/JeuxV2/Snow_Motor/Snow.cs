﻿using System;
using JeuxV2.State_game.Motor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.Snow_Motor
{
    class Snow : Objet
    {
        public Snow(Vector2 Position, ContentManager content)
        {
            Gravity = 5;
            PosiSprite = Position;
            SpriteActuel = content.Load<Texture2D>("Sprites/snow");
        }
    }
}
