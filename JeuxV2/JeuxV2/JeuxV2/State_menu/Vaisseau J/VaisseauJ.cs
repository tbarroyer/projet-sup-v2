﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeuxV2.State_shoot.Lasers;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu.Vaisseau_J
{
    public class vaisseauJ
    {

     protected struct particule
        {
            public Vector2 pos;
            public int compt;
        }

   
       
   
        protected Texture2D imgParticule;

        protected particule[] myParticulesTab;


        protected Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }


        protected Texture2D shipAct;

        public Texture2D ShipAct
        {
            get { return shipAct; }
        }

        protected int vie;

        public int Vie
        {
            get { return vie; }
        }


        public Texture2D game_over;

        protected SpriteFont font;

        protected Texture2D[] ship;

        protected int time_laps;
        protected int laser_speed;

        protected int speed;

        protected int comptL;

        protected int comptR;

        public void Init(ContentManager content)
        {

            LoadGameOver(content);

            font = content.Load<SpriteFont>("Font/SpriteFont2");

            comptL = 0;
            comptR = 0;

            myParticulesTab = new particule[30];

            imgParticule = content.Load<Texture2D>("Explosions/particule");

            Random r = new Random();

            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt = r.Next(30);
                myParticulesTab[i].pos = new Vector2();
                myParticulesTab[i].pos.X = position.X + r.Next(-10, 10);
                myParticulesTab[i].pos.Y = position.Y + r.Next(10);
            }

           
        }

        public void LoadGameOver(ContentManager content)
        {
            game_over = content.Load<Texture2D>("gameover");
        }

        public void Update(GraphicsDevice device, Laser_Manager LM, ContentManager content, GameTime time)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (time_laps >= 500)
                {
                  
                    {
                        LM.Add(new Vector2(position.X + shipAct.Width / 2, position.Y - shipAct.Height), content, laser_speed*device.Viewport.Width/800, Color.White);
                    }
                    time_laps = 0;
                }
            }
            time_laps += (int) time.ElapsedGameTime.TotalMilliseconds;
            deplacement(device);
            collision(device);

            Random r = new Random();

           
            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt++;

                int t = 10;
                t += r.Next(-7, 7);

                if (myParticulesTab[i].compt > t)
                {
                    myParticulesTab[i].pos.X = position.X + 11;
                    myParticulesTab[i].pos.Y = position.Y + shipAct.Height - 4;
                    myParticulesTab[i].compt = 0;
                }

                myParticulesTab[i].pos.X += r.Next(-5, 5);
                myParticulesTab[i].pos.Y += 5;

            }
        }

        public void Draw(SpriteBatch sb)
        {
            if (vie == 0)
                sb.Draw(game_over, new Vector2(0, 0), Color.White);
           

            foreach (var particule in myParticulesTab)
            {
                sb.Draw(imgParticule, particule.pos, Color.White);
            }

            sb.Draw(shipAct, position, Color.White);



        }


        private void deplacement(GraphicsDevice device)
        {
            shipAct = ship[0];



            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                position.X -= speed*device.Viewport.Width/800;
                shipAct = ship[1];
                comptL++;

                if (comptL > 10)
                {
                    shipAct = ship[2];
                }
            }
            else comptL = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                position.X += speed*device.Viewport.Width/800;
                shipAct = ship[3];
                comptR++;
                if (comptR > 10)
                {
                    shipAct = ship[4];
                }
            }
            else comptR = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                position.Y -= speed*device.Viewport.Width/800;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                position.Y += speed*device.Viewport.Width/800;
            }
        }

        private void collision(GraphicsDevice device)
        {
            if (position.Y > device.Viewport.Height - shipAct.Height)
            {
                position.Y = device.Viewport.Height - shipAct.Height;
            }

            if (position.Y < 0)
            {
                position.Y = 0;
            }

            if (position.X > device.Viewport.Width - shipAct.Width)
            {
                position.X = device.Viewport.Width - shipAct.Width;
            }

            if (position.X < 0)
            {
                position.X = 0;
            }
        }

  
        public bool CollisionlaserJ(Laser m, vaisseauJ joueur)
        {
            if (!((m.Position.X >= joueur.Position.X + joueur.shipAct.Width)
                  || (m.Position.X + m.Sprite_laser.Width <= joueur.Position.X)
                  || (m.Position.Y >= joueur.Position.Y + joueur.shipAct.Height)
                  || (m.Position.Y + m.Sprite_laser.Height <= joueur.Position.Y)
                ))
            {
                return true;
            }
            return false;
        }
    }
}
