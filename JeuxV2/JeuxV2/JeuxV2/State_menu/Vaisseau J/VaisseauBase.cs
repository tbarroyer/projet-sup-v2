﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_menu.Vaisseau_J
{
  
            public class VaisseauBase : vaisseauJ
    {
        public VaisseauBase(ContentManager content)
        {
            ship = new Texture2D[5];
            ship[0] = content.Load<Texture2D>("Ship/Base/ship");
            ship[1] = content.Load<Texture2D>("Ship/Base/SpriteL");
            ship[2] = content.Load<Texture2D>("Ship/Base/SpriteL2");
            ship[3] = content.Load<Texture2D>("Ship/Base/SpriteR");
            ship[4] = content.Load<Texture2D>("Ship/Base/SpriteR2");
            shipAct = ship[0];
            vie = 3;
            speed = 8;
            laser_speed = 10;

            position = new Vector2((800 - shipAct.Width) / 2, 400);

            base.Init(content);
        }
    }
}
