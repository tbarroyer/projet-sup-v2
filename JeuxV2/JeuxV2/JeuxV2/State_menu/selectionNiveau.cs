﻿using System;
using JeuxV2.Sauvegardes;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    internal class Selection : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//

        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;

        private Texture2D _arrow;
        private Vector2 _posArrow;
        private KeyboardState _oldkeyboardState;

        private Texture2D back;
        private Texture2D ship;
        private Vector2[] positionShip;
        private int selectlvl;
        private bool _mp;

        private SauvegardesManager.SaveGameData data;

        //FONCTIONS

        public Selection(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "selection", content, spriteBatch)
        {

        }

        public override bool Init(KeyboardState oldkeyboardState)
        {
            back = Content.Load<Texture2D>("backselect");
            ship = Content.Load<Texture2D>("Sprites/Ship/Base/ship");
            selectlvl = 0;

            positionShip = new Vector2[16];
            positionShip[0] = new Vector2((float)(Device.Viewport.Width * 0.16875)-ship.Width/2f, (float) (Device.Viewport.Height * 0.258)-ship.Height/2f);
            positionShip[1] = new Vector2((float)(Device.Viewport.Width * 0.227)-ship.Width/2f, (float)(Device.Viewport.Height * 0.4092)-ship.Height/2f);
            positionShip[2] = new Vector2((float)(Device.Viewport.Width * 0.2744) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.48796) - ship.Height / 2f);
            positionShip[3] = new Vector2((float)(Device.Viewport.Width * 0.3109) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.566) - ship.Height / 2f);
            positionShip[4] = new Vector2((float)(Device.Viewport.Width * 0.3218) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.63148) - ship.Height / 2f);
            positionShip[5] = new Vector2((float)(Device.Viewport.Width * 0.32812) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.71296) - ship.Height / 2f);
            positionShip[6] = new Vector2((float)(Device.Viewport.Width * 0.368229) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.80462) - ship.Height / 2f);
            positionShip[7] = new Vector2((float)(Device.Viewport.Width * 0.4781) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.83055) - ship.Height / 2f);
            positionShip[8] = new Vector2((float)(Device.Viewport.Width * 0.585416) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.777777) - ship.Height / 2f);
            positionShip[9] = new Vector2((float)(Device.Viewport.Width * 0.60260) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.68888) - ship.Height / 2f);
            positionShip[10] = new Vector2((float)(Device.Viewport.Width * 0.5916) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.59351) - ship.Height / 2f);
            positionShip[11] = new Vector2((float)(Device.Viewport.Width * 0.56666) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.531481) - ship.Height / 2f);
            positionShip[12] = new Vector2((float)(Device.Viewport.Width * 0.59166) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.42962) - ship.Height / 2f);
            positionShip[13] = new Vector2((float)(Device.Viewport.Width * 0.6401041) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.36481) - ship.Height / 2f);
            positionShip[14] = new Vector2((float)(Device.Viewport.Width * 0.69375) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.327777) - ship.Height / 2f);
            positionShip[15] = new Vector2((float)(Device.Viewport.Width * 0.81145) - ship.Width / 2f, (float)(Device.Viewport.Height * 0.237037) - ship.Height / 2f);

            /*


            positionShip[0] = new Vector2((float)(Device.Viewport.Width * 0.16875), (float)(Device.Viewport.Height * 0.258));
            positionShip[1] = new Vector2(170, 170);
            positionShip[2] = new Vector2(205, 210);
            positionShip[3] = new Vector2(230, 240);
            positionShip[4] = new Vector2(242, 270);
            positionShip[5] = new Vector2(248, 310);
            positionShip[6] = new Vector2(283, 350);
            positionShip[7] = new Vector2(370, 350);
            positionShip[8] = new Vector2(452, 338);
            positionShip[9] = new Vector2(465, 295);
            positionShip[10] = new Vector2(460, 255);
            positionShip[11] = new Vector2(440, 225);
            positionShip[12] = new Vector2(460, 180);
            positionShip[13] = new Vector2(497, 150);
            positionShip[14] = new Vector2(540, 130);
            positionShip[15] = new Vector2(650, 130);
            */
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");

            _arrow = Content.Load<Texture2D>("Sprites/arrow");
            _posArrow = new Vector2(5, 70);
            _oldkeyboardState = oldkeyboardState;

            SpriteBatch = new SpriteBatch(Device);

            data = SauvegardesManager.LoadCurrentSave();
            if (data.isMultiplayer)
                _mp = true;
            else _mp = false;
            return base.Init(oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            
            SpriteBatch.Draw(back,new Rectangle(0,0,Device.Viewport.Width,Device.Viewport.Height),Color.White);

            SpriteBatch.Draw(ship, positionShip[selectlvl], Color.White);

            string s;

            if (SauvegardesManager.LoadCurrentSave().UnlockLvl[selectlvl])
                s = "oui";
            else s = "non";

            SpriteBatch.DrawString(_font, "Niveau debloque: " + s, new Vector2(200, 10), Color.White);

            SpriteBatch.DrawString(_font, "Touche A pour rejoindre le magasin", new Vector2(100, 450), Color.White);

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && _oldkeyboardState.IsKeyUp(Keys.Enter) && !_mp)
            {
                switch (selectlvl)
                {
                    case 0:
                        if (data.UnlockLvl[0])
                            StateManager.goto_state("gamep", Keyboard.GetState(), "level2.txt", 0);
                        break;
                    case 1:
                        if (data.UnlockLvl[1])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 1);
                        break;

                    case 2:
                        if (data.UnlockLvl[2])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl2.txt", 2);
                        break;
                    case 3:
                        if (data.UnlockLvl[3])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl3.txt", 3);
                        break;
                    case 4:
                        if (data.UnlockLvl[4])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 4);
                        break;
                    case 5:
                        if (data.UnlockLvl[5])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 5);
                        break;
                    case 6:
                        if (data.UnlockLvl[6])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 6);
                        break;

                    case 7:
                        if (data.UnlockLvl[7])
                            StateManager.goto_state("gamep", Keyboard.GetState(), "level3.txt", 7);
                        break;

                    case 8:
                        if (data.UnlockLvl[8])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 8);
                        break;

                    case 9:
                        if (data.UnlockLvl[9])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 9);
                        break;

                    case 10:
                        if (data.UnlockLvl[10])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 10);
                        break;

                    case 11:
                        if (data.UnlockLvl[11])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 11);
                        break;

                    case 12:
                        if (data.UnlockLvl[12])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 12);
                        break;

                    case 13:
                        if (data.UnlockLvl[13])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 13);
                        break;

                    case 14:
                        if (data.UnlockLvl[14])
                            StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 14);
                        break;

                    case 15:
                        if (data.UnlockLvl[15])
                            StateManager.goto_state("gamep", Keyboard.GetState(), "level1.txt", 15);
                        break;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && _oldkeyboardState.IsKeyUp(Keys.Enter) && _mp)
                {
                    switch (selectlvl)
                    {
                        case 0:
                            if (data.UnlockLvl[0])
                                StateManager.goto_state("gamep2j", Keyboard.GetState(), "level2.txt", 0);
                            break;
                        case 1:
                            if (data.UnlockLvl[1])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 1);
                            break;

                        case 2:
                            if (data.UnlockLvl[2])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl2.txt", 2);
                            break;
                        case 3:
                            if (data.UnlockLvl[3])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl3.txt", 3);
                            break;
                        case 4:
                            if (data.UnlockLvl[4])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 4);
                            break;
                        case 5:
                            if (data.UnlockLvl[5])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 5);
                            break;
                        case 6:
                            if (data.UnlockLvl[6])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 6);
                            break;

                        case 7:
                            if (data.UnlockLvl[7])
                                StateManager.goto_state("gamep2j", Keyboard.GetState(), "level3.txt", 7);
                            break;

                        case 8:
                            if (data.UnlockLvl[8])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 8);
                            break;

                        case 9:
                            if (data.UnlockLvl[9])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 9);
                            break;

                        case 10:
                            if (data.UnlockLvl[10])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 10);
                            break;

                        case 11:
                            if (data.UnlockLvl[11])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 11);
                            break;

                        case 12:
                            if (data.UnlockLvl[12])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 12);
                            break;

                        case 13:
                            if (data.UnlockLvl[13])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 13);
                            break;

                        case 14:
                            if (data.UnlockLvl[14])
                                StateManager.goto_state("state_shoot", Keyboard.GetState(), "lvl1.txt", 14);
                            break;

                        case 15:
                            if (data.UnlockLvl[15])
                                StateManager.goto_state("gamep2j", Keyboard.GetState(), "level1.txt", 15);
                            break;
                    }
                }

                if (Keyboard.GetState().IsKeyDown(Keys.A) && _oldkeyboardState.IsKeyUp(Keys.A))
                {
                    StateManager.goto_state("achat", Keyboard.GetState());
                }
                if (Keyboard.GetState().IsKeyDown(Keys.R) && _oldkeyboardState.IsKeyUp(Keys.R))
                {
                    StateManager.goto_state("charger", Keyboard.GetState(), new VaisseauBase(Content),
                                            new Laser_Manager());
                }

                if (Keyboard.GetState().IsKeyDown(Keys.Right) && _oldkeyboardState.IsKeyUp(Keys.Right) &&
                    selectlvl < (positionShip.GetLength(0) - 1))
                    selectlvl++;

                if (Keyboard.GetState().IsKeyDown(Keys.Left) && _oldkeyboardState.IsKeyUp(Keys.Left) && selectlvl > 0)
                    selectlvl--;

                if (Keyboard.GetState().IsKeyDown(Keys.M) && _oldkeyboardState.IsKeyUp(Keys.M))
                {
                    StateManager.goto_state("arcade", _oldkeyboardState);
                }

                if (Keyboard.GetState().IsKeyDown(Keys.X) && _oldkeyboardState.IsKeyUp(Keys.X))
                {
                    StateManager.goto_state("gamep2j", Keyboard.GetState(), "level2.txt", 0);
                }

                _oldkeyboardState = Keyboard.GetState();

                base.Update(gameTime, graphics);

            }


        }
    }
