﻿using System.IO;
using JeuxV2.Sauvegardes;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    class SelectionJoueur : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//


        //VARIABLES
        private SpriteFont _font4;
        private VaisseauBase _vaisseau;
        private KeyboardState _oldkeyboardState;
        private Laser_Manager _LM;
        private Rectangle _Name;
        private string _TitleString;
        private Box_Manager _boxManager;
        private Rectangle _returnRectangle;
        private Rectangle _Place1;
        private Texture2D _skin1_1;
        private Texture2D _skin1_2;
        private Texture2D _skin2_1;
        private Texture2D _skin2_2;
        private int time;
        private Rectangle _Place2;

        private Texture2D back;
        private string _ReturnString;
        //FONCTIONS
        public SelectionJoueur(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "selectionJoueur", content, spriteBatch)
        {
          
            type = "menu";
        }

        public override bool Init(KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _skin1_1 = Content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite1");
            _skin1_2 = Content.Load<Texture2D>("Sprites/Caracters/Alexandre/Sprite4");
            _skin2_1 = Content.Load<Texture2D>("Sprites/Caracters/Deniz/sprite1");
            _skin2_2 = Content.Load<Texture2D>("Sprites/Caracters/Deniz/sprite2");
            _vaisseau = vaisseau;
            _boxManager = new Box_Manager(Content);
            _LM = new Laser_Manager();
            _oldkeyboardState = oldkeyboardState;
            _ReturnString = "RETOUR";
            _returnRectangle = new Rectangle((int)(Device.Viewport.Width / 90f), (int)(Device.Viewport.Height / 10f),
                                            (int)(Device.Viewport.Width / 10f), (int)(Device.Viewport.Height / 15f));
            _Name = new Rectangle((int)(Device.Viewport.Width / 4f), (int)(Device.Viewport.Height / 15f),
                                  (int)(Device.Viewport.Width / 2f), (int)(Device.Viewport.Height / 9f));
            SpriteBatch = new SpriteBatch(Device);
            _boxManager = new Box_Manager(Content);
            _boxManager.Add(_returnRectangle, Color.LightGreen);
            _boxManager.Add(_Name, Color.LightGray);
           
            _TitleString = "Sélectionnez un skin";
            _Place1 = new Rectangle((int)(Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 3f), (int)(3 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _Place2 = new Rectangle((int)(7 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 3f), (int)(3 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _boxManager.Add(_Place2, Color.Gray);
            _boxManager.Add(_Place1, Color.Gray);
            return base.Init(_oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), Color.White);

            _boxManager.DrawBox(Device, SpriteBatch);
            DrawString.DrawtheString(SpriteBatch, _font4, _ReturnString, _returnRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _TitleString, _Name, Color.Black);

            _vaisseau.Draw(SpriteBatch);
            _LM.Draw(SpriteBatch);
           
            {
                if (time > 1500)
                {
                    SpriteBatch.Draw(_skin1_1, _Place1, Color.White);
                    SpriteBatch.Draw(_skin2_2, _Place2, Color.White);
                }


                else
                {
                    if (time > 1000)
                    {


                        SpriteBatch.Draw(_skin1_2, _Place1, Color.White);
                        SpriteBatch.Draw(_skin2_2, _Place2, Color.White);
                    }
                    else
                    {
                        if (time > 500)
                        {


                            SpriteBatch.Draw(_skin1_2, _Place1, Color.White);
                            SpriteBatch.Draw(_skin2_1, _Place2, Color.White);
                        }
                        else
                        {

                            {


                                SpriteBatch.Draw(_skin1_1, _Place1, Color.White);
                                SpriteBatch.Draw(_skin2_1, _Place2, Color.White);
                            }
                        }
                    }
                }
                 if (time <= 0)
            {
                time = 2000;
            }
            


            }
            time = (int)(time - gameTime.ElapsedGameTime.TotalMilliseconds);
        

        SpriteBatch.End();
                base.Draw(gameTime);
            }
        

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (_LM.R2 == (_returnRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.go_back(_oldkeyboardState,_vaisseau,_LM);

            }
            if (_LM.R2 == _Place1)
            {
                SauvegardesManager.SaveGameData data = SauvegardesManager.LoadCurrentSave();
                data.perso.TypeJoueur = 1;
                SauvegardesManager.SaveCurrentSave(data);
                StateManager.goto_state("selection",_oldkeyboardState);
            }
            if (_LM.R2 == _Place2)
            {           
                SauvegardesManager.SaveGameData data = SauvegardesManager.LoadCurrentSave();
                data.perso.TypeJoueur = 2;
                SauvegardesManager.SaveCurrentSave(data);
                StateManager.goto_state("selection", _oldkeyboardState);
            }

            
            _oldkeyboardState = Keyboard.GetState();
            _vaisseau.Update(Device, _LM, Content, gameTime);
            _LM.Update(_boxManager);
            base.Update(gameTime, graphics);
        }

    }
}
