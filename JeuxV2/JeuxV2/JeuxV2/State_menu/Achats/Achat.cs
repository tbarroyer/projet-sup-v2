﻿using System.Collections.Generic;
using JeuxV2.Sauvegardes;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Achats;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    class Achat : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//

        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;
        private Texture2D _rectangle1;
        private Texture2D _rectangle2;
        private Texture2D _rectangle3;
        private Rectangle _rectangle31;
        private Texture2D _arrow;
        private Texture2D _coin;
        private Texture2D _shield;
        private Texture2D _doublejump;
        private Texture2D _doublelaser;
        private Texture2D _propulseur;
        private Texture2D _Ligne;
        private Vector2 _posArrow;
        private RoundedRectangle Rectangles;
        private KeyboardState _oldkeyboardState;
        private Rectangle _returnRectangle;
        private Rectangle _TitleRectangle;
        private int nmpieces;
        private int time;
        private int time2;
        private bool timeset;
        private string _ReturnString;
        private string _TitleString;
        private string _TitleString2;
        private string _TitleString3;
        private string _TitleString4;
        private string _TitleString5;
        private string _TitleString6;
        private string _currentTitle;
        private Color _currentColor;
        private string _Label1;
        private string _Label2;
        private string _Label3;
        private string _Legende1;
        private string _Legende2;
        private string _Legende3;
        
         private string _Legende4;
         private string _sLegende;
        private Rectangle _rlegende1;
        private Rectangle _r1;
        private Rectangle _r2;
        private Rectangle _r3;
        private Rectangle _rcoin;
        private Box_Manager _boxManager;
        private Box_Manager _boxManager2;
       

        private SauvegardesManager.SaveGameData data;
   
        private Vector2 cursorPos;
        private MouseState mouseState;
        private MouseState _oldMouseState;
        private Rectangle _r4;

        private Rectangle _rectangle21;
        private Rectangle _rectangle22;
        private Rectangle _rectangle23;
        private Rectangle _rectangle24;
        private Rectangle _rectangle25;
        private Rectangle _rectangle15;
        private Rectangle _rectangle14;
        private Rectangle _rectangle13;
        private Rectangle _rectangle12;
        private Rectangle _rectangle11;
        //FONCTIONS

        public Achat(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "achat", content, spriteBatch)
        {
            type = "menu";
          
        }
        public void Testperso()
        {
            if (data.perso.DoubleSaut)
            {
                _boxManager.AddBoth(_rectangle21, Color.Purple, 3);


                if (data.perso.Propulseur)
                {
                    _boxManager.AddBoth(_rectangle22, Color.Purple, 4);
                }
                else
                {
                    _boxManager.AddBoth(_rectangle22, Color.Green, 4);
                }
            }

            else
            {


                _boxManager.AddBoth(_rectangle22, Color.Silver, 4);

                _boxManager.AddBoth(_rectangle21, Color.Green, 3);
            }

        }
        public void testvaisseau()
        {

            if (data.perso.Shield)
            {
                _boxManager.AddBoth(_rectangle11, Color.Purple, 1);


                if (data.perso.Dblaser)
                {
                    _boxManager.AddBoth(_rectangle12, Color.Purple, 2);
                }
                else
                {
                    _boxManager.AddBoth(_rectangle12, Color.Green, 2);
                }
            }

            else
            {


                _boxManager.AddBoth(_rectangle12, Color.Silver, 2);

                _boxManager.AddBoth(_rectangle11, Color.Green, 1);
            }
            
        }
        public override bool Init(KeyboardState oldkeyboardState)
        {
            
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            Rectangles=new RoundedRectangle();
            _ReturnString = "RETOUR";
            _rectangle1 = Rectangles.CreateRoundedRectangleTexture(Device, (int) (Device.Viewport.Width/2f),(int) (0.66f*Device.Viewport.Height), 3, 15, 2,
                                                                         new List<Color>(){Color.Blue,Color.Black,Color.Blue,Color.Black},
                                                                         new List<Color>(){Color.White,Color.Black,Color.Gray}, 2, 2);
            _rectangle2 = Rectangles.CreateRoundedRectangleTexture(Device, (int)(Device.Viewport.Width / 2f), (int)(0.66f * Device.Viewport.Height), 3, 15, 2,
                                                                         new List<Color>() { Color.Green, Color.Black, Color.Green, Color.Black },
                                                                         new List<Color>() { Color.White, Color.Black, Color.Gray }, 2, 2);
            _rectangle3 = Rectangles.CreateRoundedRectangleTexture(Device, (int)(Device.Viewport.Width), (int)(0.34f * Device.Viewport.Height), 3, 15, 2,
                                                                       new List<Color>() { Color.Orange, Color.Black, Color.Orange, Color.Black },
                                                                       new List<Color>() { Color.White, Color.Black, Color.Gray }, 2, 2);
    
            _rcoin=new Rectangle((int) (7*Device.Viewport.Width/8f),(int) (Device.Viewport.Height/100f), (int) (Device.Viewport.Width/30f),(int) (Device.Viewport.Height/25f));
            _r1=new Rectangle((int) (Device.Viewport.Width/30f),(int) (Device.Viewport.Height/20f),(int) (Device.Viewport.Width/6f),(int) (Device.Viewport.Height/15f));
            _r2 = new Rectangle(16*(int)(Device.Viewport.Width / 30f), (int)(Device.Viewport.Height / 20f), (int)(Device.Viewport.Width / 6f), (int)(Device.Viewport.Height / 12f));
            _r3=new Rectangle((int) (4*Device.Viewport.Width/10f),(int) (2*Device.Viewport.Height/3f),(int) (Device.Viewport.Width/5f),(int) (Device.Viewport.Height/12f));
            _rectangle31 = new Rectangle((int)(0.10 * Device.Viewport.Width), (int)(0.75 * Device.Viewport.Height), (int)(0.064 * Device.Viewport.Width), (int)(0.133 * Device.Viewport.Height));
            _rectangle11 = new Rectangle((int)(0.21961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.5615),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle12 = new Rectangle((int)(0.21961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.43974),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle13 = new Rectangle((int)(0.21961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.31794),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle14 = new Rectangle((int)(0.21961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.196153),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle15 = new Rectangle((int)(0.21961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.0743),
                                        (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle21 = new Rectangle((int)(0.71961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.5615),
                                        (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle22 = new Rectangle((int)(0.71961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.43974),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle23 = new Rectangle((int)(0.71961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.31794),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle24 = new Rectangle((int)(0.71961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.196153),
                                         (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
            _rectangle25 = new Rectangle((int)(0.71961 * Device.Viewport.Width), (int)(Device.Viewport.Height * 0.0743),
                                        (int)(0.06078 * Device.Viewport.Width), (int)(0.0974 * Device.Viewport.Height));
        
            _r4 = new Rectangle((int) (9*Device.Viewport.Width/10f), 0, (int) (Device.Viewport.Width/10f), (int) (Device.Viewport.Height/20f));
            _TitleString = "Bienvenue dans la boutique !";
            _TitleString2 = "Utilisez votre souris";
            _TitleString3 = "Vous n'avez pas assez de pièces !";
            _TitleString4 = "Vous avez déjà débloqué ceci";
            _TitleString5 = "Merci d'avoir acheté : ";
            _TitleString6 = "Vous n'avez pas encore débloqué ceci";
            _Label1 = "Compétences Vaisseau";
            _Label2 = "Compétences Personnage";
            _Label3 = "Consommables";
            _coin = Content.Load<Texture2D>("Sprites/Power Up/Coins/Coin1");
            _TitleRectangle = new Rectangle((int) (Device.Viewport.Width/4f),0,
                                  (int) (Device.Viewport.Width/2f), (int) (Device.Viewport.Height/15f));
            _shield = Content.Load<Texture2D>("bouclier");
            _doublejump = Content.Load<Texture2D>("Sprites/DoubleJump");
            _arrow = Content.Load<Texture2D>("Sprites/curseur");
            _doublelaser = Content.Load<Texture2D>("Sprites/Doublelaser");
            _propulseur  = Content.Load<Texture2D>("Sprites/Propulseur");

            _posArrow = new Vector2(5, 70);
            _Ligne = new Texture2D(Device, 3, 3, false, SurfaceFormat.Color);
            _Ligne.SetData(new[] { Color.White,Color.White,Color.White,Color.White, 
            Color.White,Color.White,Color.White,Color.White,
            Color.White});
     
           
            _oldkeyboardState = oldkeyboardState;
            _returnRectangle = new Rectangle(0, 0, Device.Viewport.Width/10, (int) (Device.Viewport.Height/15f));
            _boxManager = new Box_Manager(Content);
            _boxManager2 = new Box_Manager(Content);
            _boxManager.AddBoth(_rectangle13, Color.Silver, 15);
            _boxManager.AddBoth(_rectangle14, Color.Silver, 19);
            _boxManager.AddBoth(_rectangle15, Color.Silver, 5);
            _boxManager.AddBoth(_rectangle23, Color.Silver, 8);
            _boxManager.AddBoth(_rectangle24, Color.Silver, 9);
            _boxManager.AddBoth(_rectangle25, Color.Silver, 10);
            _boxManager.AddBoth(_rectangle31, Color.Silver, 11);                 
            SpriteBatch = new SpriteBatch(Device);
            _boxManager.Add(_returnRectangle);
            data = SauvegardesManager.LoadCurrentSave();
 
            nmpieces = data.nmPièces;
            
        testvaisseau();
            Testperso();

            
            mouseState = Mouse.GetState();
            timeset = false;
            #region legendes

            _Legende1 =
                "Bouclier\n s'active en ramassant 5 gemmes\n Prix : 20 pièces";
            _Legende2 = "Double laser\n double vos attaques!\n Prix : 100 pièces";
            _Legende3 = "Double saut\n Appuyez sur Espace en l'air\n pour sauter plus haut!\n Prix : 50 pièces";
            _Legende4 = "Propulseur\n Appuyez sur V en chute\n pour planer\n Prix : 50 pièces";


            #endregion legendes
            return base.Init(oldkeyboardState);


        }

        public override void Draw(GameTime gameTime)
        {

            Device.Clear(Color.DarkBlue);
     
            SpriteBatch.Begin();
            SpriteBatch.Draw(_arrow, _posArrow, Color.White);
            SpriteBatch.Draw(_rectangle1,new Vector2(0,Device.Viewport.Height/20f),Color.White);
            SpriteBatch.Draw(_rectangle2, new Vector2(Device.Viewport.Width/2f, Device.Viewport.Height/20f), Color.White);
            SpriteBatch.Draw(_rectangle3, new Vector2(0, Device.Viewport.Height*0.66f), Color.White);
           _boxManager.DrawBox(Device, SpriteBatch);
          
           SpriteBatch.Draw(_coin,_rcoin,null,Color.White);
            SpriteBatch.Draw(_shield, _rectangle11, null, Color.White);
            SpriteBatch.Draw(_doublejump, _rectangle21, null, Color.White);
            SpriteBatch.Draw(_doublelaser, _rectangle12, null, Color.White);
            SpriteBatch.Draw(_propulseur, _rectangle22, null, Color.White);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle11.Center.X, _rectangle11.Top), new Vector2(_rectangle11.Center.X, _rectangle12.Bottom), Color.Red,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle11.Center.X, _rectangle12.Top), new Vector2(_rectangle11.Center.X, _rectangle13.Bottom), Color.Red,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle11.Center.X, _rectangle13.Top), new Vector2(_rectangle11.Center.X, _rectangle14.Bottom), Color.Red,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle11.Center.X, _rectangle14.Top), new Vector2(_rectangle11.Center.X, _rectangle15.Bottom), Color.Red,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle21.Center.X, _rectangle21.Top), new Vector2(_rectangle21.Center.X, _rectangle22.Bottom), Color.Blue,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle21.Center.X, _rectangle22.Top), new Vector2(_rectangle22.Center.X, _rectangle23.Bottom), Color.Blue,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle21.Center.X, _rectangle23.Top), new Vector2(_rectangle23.Center.X, _rectangle24.Bottom), Color.Blue,3);
            Line_Creator.DrawLineTo(SpriteBatch, _Ligne, new Vector2(_rectangle21.Center.X, _rectangle24.Top), new Vector2(_rectangle23.Center.X, _rectangle25.Bottom), Color.Blue,3);
            
       
       
       
       
       
       
       
       
          //  SpriteBatch.DrawString(_font, "Achat", new Vector2(10, 20), Color.Black);
            DrawString.DrawtheString(SpriteBatch,_font4,_ReturnString,_returnRectangle,Color.LightGray);
            DrawString.DrawtheString(SpriteBatch,_font4,_Label1,_r1,Color.White);
            DrawString.DrawtheString(SpriteBatch, _font4, _Label2, _r2, Color.White);
            DrawString.DrawtheString(SpriteBatch, _font4, _Label3, _r3, Color.White);
            DrawString.DrawtheString(SpriteBatch, _font4, nmpieces.ToString(), _r4, Color.Yellow);

            
            if (time <= 0)
            {
                time = 6000;
            }
            else
            {
                if (time <= 3000)
                {
                    DrawString.DrawtheString(SpriteBatch, _font4, _TitleString, _TitleRectangle, Color.Beige);
                }
                else if(time<=6000)
                {

                    DrawString.DrawtheString(SpriteBatch, _font4, _TitleString2, _TitleRectangle, Color.Orange);
                }
                else DrawString.DrawtheString(SpriteBatch, _font4, _currentTitle, _TitleRectangle, _currentColor);
            }
            time = (int)(time - gameTime.ElapsedGameTime.TotalMilliseconds);
            time2  = (int)(time2 - gameTime.ElapsedGameTime.TotalMilliseconds);
          //  SpriteBatch.DrawString(_font4, "Propulseur: " + s + " (50 pieces)", new Vector2(50, 70), Color.Black);

            SpriteBatch.Draw(_arrow, cursorPos, Color.White);
            _boxManager2.DrawBox(Device, SpriteBatch);
            _boxManager2.Remove(_rlegende1);
            if(time2 <= 0 && timeset)
            {
                
            
               
      
                _rlegende1 = new Rectangle(mouseState.X, mouseState.Y, Device.Viewport.Width / 4,
                                           (int)(Device.Viewport.Height / 8f));
                DrawString.DrawtheString(SpriteBatch, _font4, _sLegende, _rlegende1, Color.Black);
         
                _boxManager2.Add(_rlegende1, Color.White);
        
               



            
            }
         
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            nmpieces = data.nmPièces;
            mouseState = Mouse.GetState();
            
            cursorPos = new Vector2(mouseState.X, mouseState.Y);


            if (mouseState.LeftButton == ButtonState.Pressed && _oldMouseState.LeftButton == ButtonState.Released)
            {

                {

                }
                if (_boxManager.Collisionsouris(mouseState) == _returnRectangle)
                {
                    StateManager.go_back(_oldkeyboardState);
                }
                else
                {
                    switch (_boxManager.Collisionsouris_int(mouseState))
                    {

                           
                        case 1: 
                            if (data.perso.Shield)
                            {
                                time = 8000;
                                _currentTitle = _TitleString4;
                                _currentColor = Color.Red;
                            }
                            else
                            {
                                if (nmpieces < 10)
                                {
                                    time = 8000;
                                    _currentTitle = _TitleString3;
                                    _currentColor = Color.Red;
                                }

                                else
                                {
                                    time = 8000;
                                    data.perso.Shield = true;
                                    data.nmPièces -= 20;
                                    _currentColor = Color.Green;
                                    _currentTitle = _TitleString5 + "Bouclier";
                                    SauvegardesManager.SaveCurrentSave(data);
                                    _boxManager.Remove(_rectangle11);
                                    _boxManager.Remove(_rectangle12);
                                    testvaisseau();
                                }
                            }
                          
                            break;
                        case 2: 
                             if (!data.perso.Shield )
                            {
                                time = 8000;
                                _currentTitle = _TitleString6;
                                _currentColor = Color.Red;
                            }
                             else
                             { 
                                 if (data.perso.Dblaser)
                             {
                                 time = 8000;
                                 _currentTitle = _TitleString4;
                                 _currentColor = Color.Red;
                             }
                               
                             else
                                 {
                                     if (nmpieces < 100)
                                     {
                                         time = 8000;
                                         _currentTitle = _TitleString3;
                                         _currentColor = Color.Red;
                                     }

                                     else
                                     {
                                         time = 8000;
                                         data.perso.Dblaser = true;
                                         data.nmPièces -= 100;
                                         _currentColor = Color.Green;
                                         _currentTitle = _TitleString5 + "Double Laser";
                                         SauvegardesManager.SaveCurrentSave(data);
                                         _boxManager.Remove(_rectangle11);
                                         _boxManager.Remove(_rectangle12);
                                         testvaisseau();
                                     }

                                 }
                                 
                             }
                            break;
                        case 3:
                            
                           if (data.perso.DoubleSaut)
                            {
                                time = 8000;
                                _currentTitle = _TitleString4;
                                _currentColor = Color.Red;
                            }
                           else
                           {
                               if (nmpieces < 50)
                               {
                                   time = 8000;
                                   _currentTitle = _TitleString3;
                                   _currentColor = Color.Red;
                               }
                               else
                               {
                                   time = 8000;
                                   data.perso.DoubleSaut = true;
                                   data.nmPièces -= 10;
                                   _currentColor = Color.Green;
                                   _currentTitle = _TitleString5 + "Double Saut";
                                   SauvegardesManager.SaveCurrentSave(data);
                 
                                   _boxManager.Remove(_rectangle21);
                                   _boxManager.Remove(_rectangle22);
                                   Testperso();
                               }
                           }
                                
                            break;
                        case 4: 
                            if (!data.perso.DoubleSaut)
                            {
                                time = 8000;
                                _currentTitle = _TitleString6;
                                _currentColor = Color.Red;
                            }
                            else if (data.perso.Propulseur)
                            {
                                time = 8000;
                                _currentTitle = _TitleString4;
                                _currentColor = Color.Red;
                            }
                            else
                                
                                if (nmpieces < 50)
                                {
                                    time = 8000;
                                    _currentTitle = _TitleString3;
                                    _currentColor = Color.Red;
                                }
                                else
                            {
                                time = 8000;
                                data.perso.Propulseur = true;
                                data.nmPièces -= 50;
                                _currentColor = Color.Green;
                                _currentTitle = _TitleString5+"Propulseur";
                                SauvegardesManager.SaveCurrentSave(data);
                                _boxManager.Remove(_rectangle21);
                                _boxManager.Remove(_rectangle22);
                                Testperso();
                            }
                            break;


                    }
                }
               
            }
            else if
           (
                mouseState.LeftButton == ButtonState.Released && _oldMouseState.LeftButton == ButtonState.Released)
                if(_boxManager.Collisionsouris_int(mouseState)!=-1)
                {

                switch (_boxManager.Collisionsouris_int(mouseState))
                {
                    case 1:
                        _sLegende = _Legende1;
                        break;
                        case 2:
                        _sLegende = _Legende2;
                        break;
                        case 3:
                        _sLegende = _Legende3;
                        break;
                        case 4:
                        _sLegende = _Legende4;
                        break;
                    default: _sLegende = "Bientot Disponible";
                        break;
                }
            
                    timeset = true;

                }
                else timeset = false;

            //propulseur 50 
            //double jump 50
            //double laser 100
            //

 
            _oldkeyboardState = Keyboard.GetState();
            _oldMouseState = mouseState;
            if (!timeset)
            {
                time2 = 300;
            }
            base.Update(gameTime, graphics);
        }
    }
}
