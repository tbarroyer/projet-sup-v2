﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeuxV2.State_shoot.Lasers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    public struct CRectangle
    {
        public Rectangle Rectangle;
        public Color Color;
    }

    public struct CRiRectangle
    {
        public Rectangle Rectangle;
        public Color Color;
        public int i;
    }

    public class Box_Manager
    {

        private Texture2D texture;
        private List<CRectangle> list_cr;
        private List<Rectangle> list_r;
        private List<CRiRectangle> list_cri;
        private List<Rectangle> list_ignored;
        
        private SoundEffect sound;
       
        public Box_Manager(ContentManager content)
        {
            list_cr = new List<CRectangle>();
            list_r = new List<Rectangle>();
            list_cri = new List<CRiRectangle>();
            list_ignored = new List<Rectangle>();
            sound = content.Load<SoundEffect>("Sound/missile");
        }

        private void DrawTexture(GraphicsDevice device, SpriteBatch sb, CRectangle Crectangle)
        {
            texture = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            texture.SetData(new[] {Color.White});
            sb.Draw(texture, Crectangle.Rectangle, Crectangle.Color);
        }

        private void DrawTexture(GraphicsDevice device, SpriteBatch sb, CRiRectangle Crirectangle, int j)
        {
            if (Crirectangle.i < j)
            {

                texture = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
                texture.SetData(new[] {Color.White});
                sb.Draw(texture, Crirectangle.Rectangle, Crirectangle.Color);
            }

        }

        public void DrawBox(GraphicsDevice device, SpriteBatch sb)
        {
            foreach (var Crectangle in list_cr)
            {
                DrawTexture(device, sb, Crectangle);
            }

        }

        public void DrawBox(GraphicsDevice device, SpriteBatch sb, int i)
        {


            foreach (var rectangle in list_cri)
            {
                DrawTexture(device, sb, rectangle, i);
            }
        }

        public void Remove(Rectangle Rect)
        {
            if (list_r.Contains(Rect))
            {
                list_r.Remove(Rect);
                CRectangle a = new CRectangle();
                bool b = false;
                foreach (var cRectangle in list_cr)
                {
                    if
                        (cRectangle.Rectangle == Rect)
                    {
                        a = cRectangle;
                        b = true;

                    }
                }

                {
                    if (b)
                    {
                        list_cr.Remove(a);
                    }

                }
            }
            if(list_ignored.Contains(Rect))
            {
                list_ignored.Remove(Rect);
            }


        }

        public void Add(Rectangle Rect)
        {
            list_r.Add(Rect);
        }
        public void AddIgnored(Rectangle Rect)
        {
            list_ignored.Add(Rect);
        }

        public void Add(Rectangle Rect, Color Color)
        {
            CRectangle c;
            c.Rectangle = Rect;
            c.Color = Color;
            list_cr.Add(c);
            list_r.Add(c.Rectangle);
        }

        public void Add(Rectangle Rect, Color Color, int i)
        {
            CRiRectangle c;
            c.Rectangle = Rect;
            c.Color = Color;
            c.i = i;
            list_cri.Add(c);
            list_r.Add(c.Rectangle);
        }
        public void AddBoth(Rectangle Rect, Color Color, int i)
        {
            CRiRectangle c;
            CRectangle c2;
            c.Rectangle = Rect;
            c.Color = Color;
            c2.Rectangle = Rect;
            c2.Color = Color;
            list_cr.Add(c2);
            c.i = i;
            list_cri.Add(c);
            list_r.Add(c.Rectangle);
        }

        public Rectangle Collisionlaser(Laser m)
        {
            foreach (Rectangle rect in list_r)
            {
                if (!((m.Position.X >= rect.X + rect.Width)
                      || (m.Position.X + m.Sprite_laser.Width <= rect.X)
                      || (m.Position.Y >= rect.Y + rect.Height)
                      || (m.Position.Y + m.Sprite_laser.Height <= rect.Y)
                     ))
                {
                    sound.Play();
                    return rect;
                }

            }

            return Rectangle.Empty;
        }
        public Rectangle Collisionsouris(MouseState m)
        {
            foreach (Rectangle rect in list_r)
            {
                if (!((m.X >= rect.X + rect.Width)
                      || (m.X  <= rect.X)
                      || (m.Y >= rect.Y + rect.Height)
                      || (m.Y  <= rect.Y)
                     ))
                {
                    return rect;
                }

            }

            return Rectangle.Empty;
        }

        public Rectangle CollisionRectangle(Rectangle r)
        {
         
            foreach (Rectangle rect in list_r)
            {
                if (rect!=r)
                {
                    
                if (!((r.X >= rect.X + rect.Width)
                      || (r.X + r.Width <= rect.X)
                      || (r.Y >= rect.Y + rect.Height)
                      || (r.Y + r.Height <= rect.Y)
                     ))
                {
                    sound.Play();
                    return rect;
                }
            }
            }


            return Rectangle.Empty;
        }
        public int Collisionsouris_int(MouseState m)
        {
            foreach (CRiRectangle rect in list_cri)
            {
                

                     if (!((m.X >= rect.Rectangle.X + rect.Rectangle.Width)
                      || (m.X  <= rect.Rectangle.X)
                      || (m.Y >= rect.Rectangle.Y + rect.Rectangle.Height)
                      || (m.Y <= rect.Rectangle.Y)
                     ))
                {
                    return rect.i;
                }

            }


            return -1;
        }


    }
}
