﻿using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace JeuxV2.State_menu
{
    class Menu : State
    {
        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;

        private KeyboardState _oldkeyboardState; //old keystate
        public VaisseauBase _vaisseau;
        private Rectangle _OptionsRectangle;
        private Rectangle _PlayRectangle;
        private Rectangle _ExitRectangle;
        private string _OptionsString; 
        private string _PlayString; 
        private string _LeaveString;
        private Laser_Manager _LM;

        private Texture2D back;
        private Box_Manager _boxManager;
        private Game _game;
        //FONCTIONS
        public Menu(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch,Game game)
            : base(device, "menu", content, spriteBatch)
        {
            _game = game;
        }
        public override bool Init(KeyboardState oldkeyboardState)
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            _font = Content.Load<SpriteFont>("Font/SpriteFont1"); //loading font1
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4"); //loading font2
            _oldkeyboardState = oldkeyboardState; //Getting oldkeystate
            _LM = new Laser_Manager();
            _OptionsRectangle = new Rectangle((int)(Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _PlayRectangle = new Rectangle((int)(7 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _ExitRectangle = new Rectangle((int)(13 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _boxManager=new Box_Manager(Content);
            _boxManager.Add(_PlayRectangle,Color.Blue);
            _boxManager.Add(_ExitRectangle,Color.Beige);
            _boxManager.Add(_OptionsRectangle,Color.BlueViolet);
            _OptionsString="OPTIONS";
            _PlayString = "JOUER";
            _LeaveString = "QUITTER";
            SpriteBatch = new SpriteBatch(Device);
            _vaisseau = new VaisseauBase(Content);

           

            return base.Init(oldkeyboardState);
        }
      

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);
            
            SpriteBatch.Begin();
           SpriteBatch.Draw(back, new Vector2(0, 0),null,Color.White,0f,Vector2.Zero,1,new SpriteEffects(),1 );
            _boxManager.DrawBox(Device, SpriteBatch);
            SpriteBatch.DrawString(_font, "Menu", new Vector2(40, 40), Color.White);
            DrawString.DrawtheString(SpriteBatch, _font4, _PlayString, _PlayRectangle,Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _OptionsString, _OptionsRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _LeaveString, _ExitRectangle, Color.Black);
            _vaisseau.Draw(SpriteBatch);
            _LM.Draw(SpriteBatch);
         
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (_LM.R2==(_OptionsRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("options", Keyboard.GetState(), _vaisseau, _LM);
            
            }
            if (_LM.R2== (_PlayRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("jouer", Keyboard.GetState(), _vaisseau, _LM);
                
            }
            if (_LM.R2 == (_ExitRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                _game.Exit();
            }

            base.Update(gameTime, graphics);
            _vaisseau.Update(Device,_LM,Content,gameTime);
            _LM.Update(_boxManager);
        }
    }
}
