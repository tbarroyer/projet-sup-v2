﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_menu
{
   public class Line_Creator
    {

        public static void DrawLineTo( SpriteBatch sb, Texture2D texture, Vector2 src, Vector2 dst, Color color,int width)
        {
            //direction is destination - source vectors
            Vector2 direction = dst - src;
            //get the angle from 2 specified numbers (our point)
            var angle = (float)Math.Atan2(direction.Y, direction.X);
            //calculate the distance between our two vectors
            float distance;
            Vector2.Distance(ref src, ref dst, out distance);

            //draw the sprite with rotation
            sb.Draw(texture, src, new Rectangle((int)src.X, (int)src.Y, (int)distance, width), color, angle, Vector2.Zero, 1.0f, SpriteEffects.None, 0);
        }

    }
}
