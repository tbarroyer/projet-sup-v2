﻿using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    class Jouer : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//


        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font2;
        private  VaisseauBase _vaisseau;
        private KeyboardState _oldkeyboardState;
        private Laser_Manager _LM;
        private Rectangle _NewGameRectangle;
        private Rectangle _ReturnRectangle;
        private Rectangle _LoadRectangle;
        private string _LoadString;
        private string _ReturnString;
        private string _NewGameString;
        private Box_Manager _boxManager;
        private Texture2D back;
       

        //FONCTIONS
        public Jouer(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "jouer", content, spriteBatch)
        {
        }

        public override bool Init(KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
          
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font2 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _vaisseau = vaisseau;
            _LM = LM;
            _oldkeyboardState = oldkeyboardState;
             _ReturnRectangle = new Rectangle((int)(Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
             _LoadRectangle = new Rectangle((int)(7 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _NewGameRectangle= new Rectangle((int)(13 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5 * Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _LoadString = "CHARGER";
            _NewGameString = "NOUVEAU JEU";
            _ReturnString = "RETOUR";
            _boxManager = new Box_Manager(Content);
            _boxManager.Add(_NewGameRectangle,Color.LightBlue);
            _boxManager.Add(_ReturnRectangle,Color.Beige);
            _boxManager.Add(_LoadRectangle,Color.Gray);
            SpriteBatch = new SpriteBatch(Device);

            return base.Init(_oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), Color.White);
            _boxManager.DrawBox(Device, SpriteBatch); 
            SpriteBatch.DrawString(_font, "Jouer", new Vector2(40, 40), Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font2, _NewGameString, _NewGameRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font2, _ReturnString, _ReturnRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font2, _LoadString, _LoadRectangle, Color.Black);
            _vaisseau.Draw(SpriteBatch);
            _LM.Draw(SpriteBatch);
          
            SpriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (_LM.R2 == (_NewGameRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("nouvelle", Keyboard.GetState(), _vaisseau, _LM);

            }
      if (_LM.R2 == (_LoadRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("charger", Keyboard.GetState(), _vaisseau, _LM);

            }
            if (_LM.R2 == (_ReturnRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("menu", Keyboard.GetState(), _vaisseau, _LM);

            }
            _oldkeyboardState = Keyboard.GetState();
            _vaisseau.Update(Device,_LM,Content,gameTime);
            _LM.Update(_boxManager);
            base.Update(gameTime, graphics);
        }
    }
}