﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace JeuxV2.State_menu
{
    internal class Volume : State
    {
        private KeyboardState _oldkeyboardState;
        private SpriteFont _font;

        private Texture2D back;
        private SpriteFont _font4;
        private VaisseauBase _vaisseau;
        private Laser_Manager _LM;
        private Box_Manager _boxManager;
        private string _VolumeString;
        private string _SetString;
        private string _ReturnString;
        private Rectangle _VolumeRectangle;
        private Rectangle _SetRectangle;
        private Rectangle [] rectanglelist;
        private Rectangle _BigSetRectangle;
        private Rectangle _musicRectangle;
        private Rectangle _returnRectangle;
        
            private int j;
      
        public Volume(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "volume", content, spriteBatch)

        {
        }
        public void RectangleRender(int j)
        {
            for (int i = 1; i < j; i++)
            {
                rectanglelist[i] = new Rectangle((_SetRectangle.Left + (49 - i) * (_SetRectangle.Center.X - _SetRectangle.Left) / 49), (rectanglelist[i - 1].Top - (int)(_SetRectangle.Height / 50f)), (_SetRectangle.Width * i / 49), (int)(_SetRectangle.Height / 50f));
                _boxManager.Add(rectanglelist[i], (new Color((255 * i) / 49, (255 - (255 * i) / 49), 0)),i);
            }
        }
            
        public override bool Init(KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            j = (int)MediaPlayer.Volume*49;
            _VolumeRectangle = new Rectangle((int) (Device.Viewport.Width/2f), (int) (Device.Viewport.Height/20f), (int) (Device.Viewport.Width/7f), (int) (Device.Viewport.Height/14f));
            _VolumeRectangle.X = _VolumeRectangle.Left - (_VolumeRectangle.Center.X - _VolumeRectangle.Left);
            _SetRectangle=new Rectangle((int) (2*Device.Viewport.Width/5f),(int) (2*Device.Viewport.Height/15f),(int) (Device.Viewport.Width/5f),(int) (3*Device.Viewport.Height/4f));
            _BigSetRectangle = new Rectangle((int)(1.5 * Device.Viewport.Width / 5f), (int)(1.7 * Device.Viewport.Height / 15f), (int)(2*Device.Viewport.Width /5f), (int)(3.3 * Device.Viewport.Height / 4f));
            rectanglelist = new Rectangle[50];
            _boxManager = new Box_Manager(Content);
            _musicRectangle = new Rectangle(_BigSetRectangle.Left,(int) (_BigSetRectangle.Bottom-Device.Viewport.Height/25f),_BigSetRectangle.Width,(int) (Device.Viewport.Height/25f));
            _boxManager.Add(_VolumeRectangle, Color.Blue);
            _boxManager.Add(_BigSetRectangle,Color.DarkGray);
            
            _returnRectangle = new Rectangle((int) (Device.Viewport.Width/20f), (int) (Device.Viewport.Height/10f),
                                             (int) (Device.Viewport.Width/10f), (int) (Device.Viewport.Height/15f));
          
            rectanglelist[0] = new Rectangle((int)(_SetRectangle.Left), (int)(_SetRectangle.Bottom - _SetRectangle.Height / 50f), (int)(_SetRectangle.Width), (int)(_SetRectangle.Height / 50f));
            _boxManager.Add(_returnRectangle, Color.LightGreen);
            RectangleRender(49);

         
                _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _LM = LM;
            _oldkeyboardState = oldkeyboardState;
            _vaisseau = vaisseau;
            _VolumeString = "VOLUME";
            _SetString = "MUSIQUE";
            _ReturnString = "RETOUR";
            SpriteBatch = new SpriteBatch(Device);
            
            
           vaisseau.Position= new Vector2((Device.Viewport.Width-vaisseau.ShipAct.Width)/2f,Device.Viewport.Height);
            return base.Init(_oldkeyboardState);
        }
        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), Color.White);
            _boxManager.DrawBox(Device, SpriteBatch);
        
            _boxManager.DrawBox(Device,SpriteBatch,j);
            _LM.Draw(SpriteBatch);
            DrawString.DrawtheString(SpriteBatch, _font4, _VolumeString, _VolumeRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _SetString, _musicRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _ReturnString, _returnRectangle, Color.Black);
            _vaisseau.Draw(SpriteBatch);
            SpriteBatch.End();

            base.Draw(gameTime);
        }
        public int CollisionVolume()
        {
            int i;
            for ( i = 0; i < rectanglelist.Length;i++ )
            {
                Rectangle rect = rectanglelist[i];
                if (!((_vaisseau.Position.X >= rect.X + rect.Width)
                      || (_vaisseau.Position.X + _vaisseau.ShipAct.Width <= rect.X)
                      || (_vaisseau.Position.Y >= rect.Y + rect.Height)
                      || (_vaisseau.Position.Y + _vaisseau.ShipAct.Height <= rect.Y)
                     ))
                    break;
            }
         

                return i;
                
            }

            public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {

    _oldkeyboardState = Keyboard.GetState();
            _vaisseau.Update(Device, _LM, Content, gameTime);
            _LM.Update(_boxManager);
               if (CollisionVolume()<rectanglelist.Length)  
                   j = CollisionVolume();
                MediaPlayer.Volume = j/49f;
                if (_LM.R2 == (_returnRectangle))
                {
                    _LM.R2 = Rectangle.Empty;
                    StateManager.goto_state("options", Keyboard.GetState(), _vaisseau, _LM);

                }
            base.Update(gameTime, graphics);
            
                
        }
    }
   
    }

    