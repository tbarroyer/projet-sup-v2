﻿using System.IO;
using JeuxV2.Sauvegardes;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    internal class Nouvelle : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//


        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;
        private VaisseauBase _vaisseau;
        private KeyboardState _oldkeyboardState;
        private Laser_Manager _LM;
        private Box_Manager _boxManager;
        private TextBox textbox;
        private Rectangle _returnRectangle;
        private Rectangle _Place1;
        private string _place1s;
        private Rectangle _Place2;
        private string _place2s;
        private Rectangle _Place3;
        private string _place3s;
        private Rectangle _Name, _Name2;
        private Vector2 _oldPosition;
        private Rectangle _textRectangle;
        private string _ReturnString;
        private string _TitleString;
        private int time;
        private Texture2D texture;
        private Color[] _colortable;

        private Texture2D back;
        private string _TitleString2;
        private string _TitleString3;
        private bool _isLaunched,_isMultiplayer;

        private bool[] tabSaves;
        private string _TitleString4;

        //FONCTIONS
        public Nouvelle(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "nouvelle", content, spriteBatch)
        {
            type = "menu";
        }

        public override bool Init(KeyboardState oldkeyboardState, VaisseauBase vaisseau, Laser_Manager LM)
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            _isLaunched = false;
            _colortable = new Color[25];
            for (int i = 0; i < 25;i++ )
            {
                _colortable[i] = Color.White;
            }
                tabSaves = SauvegardesManager.CheckSaves();
            _oldPosition = new Vector2();
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _vaisseau = vaisseau;
            _LM = LM;
            _oldkeyboardState = oldkeyboardState;
            _isMultiplayer = false;
            SpriteBatch = new SpriteBatch(Device);
            _returnRectangle = new Rectangle((int) (Device.Viewport.Width/90f), (int) (Device.Viewport.Height/10f),
                                             (int) (Device.Viewport.Width/10f), (int) (Device.Viewport.Height/15f));
            _Name = new Rectangle((int) (Device.Viewport.Width/4f), (int) (Device.Viewport.Height/15f),
                                  (int) (Device.Viewport.Width/2f), (int) (Device.Viewport.Height/9f));
            _Name2 = new Rectangle((int) (4 * Device.Viewport.Width / 5f), (int)(Device.Viewport.Height / 15f), (int) (Device.Viewport.Width/6f), (int) (Device.Viewport.Height/9f));
            _boxManager=new Box_Manager(Content);
            _boxManager.Add(_returnRectangle, Color.LightGreen);
            _boxManager.Add(_Name, Color.LightGray);
            _boxManager.Add(_Name2,Color.LightBlue);
            _ReturnString = "RETOUR";
            _TitleString = "Sélectionnez un emplacement";
            _TitleString2 = "Appuyez sur F1 pour démarrer la gravure";
            _TitleString3 = "Entrez votre nom puis appuyez sur F1";
            _TitleString4 = "Touche U \n Mode 2 joueurs";
            _textRectangle = new Rectangle((int) (vaisseau.Position.X-Device.Viewport.Width/8f),(int) (vaisseau.Position.Y-vaisseau.ShipAct.Height-Device.Viewport.Height/12f),(int) (Device.Viewport.Width/4f+vaisseau.ShipAct.Width), (int) (Device.Viewport.Height/12f));
            
            _Place1=new Rectangle((int) (Device.Viewport.Width/19f),(int) (Device.Viewport.Height/5f),(int) (5*Device.Viewport.Width/19f),  (int) (Device.Viewport.Height/5f));
            _Place2 = new Rectangle((int)(7*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _Place3 = new Rectangle((int)(13*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            textbox = new TextBox(Device,_font4,_textRectangle,SpriteBatch);
            //  textbox = new TextBox(Device,_font4,_textRectangle) { ForegroundColor = Color.YellowGreen, BackgroundColor = Color.White, Position = vaisseau.Position-new Vector2(0,vaisseau.ShipAct.Height), HasFocus = true };
            _boxManager.AddIgnored(_textRectangle);

            if (tabSaves[0])
            {
                _boxManager.Add(_Place1, Color.Red);
                _place1s = SauvegardesManager.LoadSave("saveslot0.sav").PlayerName;
            }
            else
            {
                _boxManager.Add(_Place1, Color.Green);
                _place1s = "Emplacement 1";

            }
            if (tabSaves[1])
            {
                _boxManager.Add(_Place2, Color.Red);
                _place2s = SauvegardesManager.LoadSave("saveslot1.sav").PlayerName;

            }
            else
            {
                _boxManager.Add(_Place2, Color.Green);
                _place2s = "Emplacement 2";

            }
            if (tabSaves[2])
            {
                _boxManager.Add(_Place3, Color.Red);
                _place3s = SauvegardesManager.LoadSave("saveslot2.sav").PlayerName;
            }
            else
            {
                _boxManager.Add(_Place3, Color.Green);
                _place3s = "Emplacement 3";
            }
            return base.Init(_oldkeyboardState);

        }

        public override void Draw(GameTime gameTime)
        {
            textbox.PreDraw();
            Device.Clear(Color.White);
            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1, new SpriteEffects(), 1f);
        
            _boxManager.DrawBox(Device, SpriteBatch);
            DrawString.DrawtheString(SpriteBatch, _font4, _ReturnString, _returnRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch,_font4,_TitleString4,_Name2,Color.Black);
            if(textbox.HasFocus==false)
            {
                
                if(time<=0)
                {
                    time = 6000;
                }
                else
                {
                    if (time <= 3000)
                    {
                        DrawString.DrawtheString(SpriteBatch, _font4, _TitleString, _Name, Color.Black);
                    }
                    else
                    {

                        DrawString.DrawtheString(SpriteBatch, _font4, _TitleString2, _Name, Color.Blue);
                    }
                
                }
                time = (int) (time - gameTime.ElapsedGameTime.TotalMilliseconds);
            }
            else
            {
                DrawString.DrawtheString(SpriteBatch, _font4, _TitleString3, _Name, Color.Orange);
                _LM.Clear();
            }
            DrawString.DrawtheString(SpriteBatch, _font4, _place1s, _Place1, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _place2s, _Place2, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _place3s, _Place3, Color.Black);
           
            if(textbox.HasFocus)
            {
                if (!_isMultiplayer)
                {
                    
 
                   texture = new Texture2D(Device, 1, 1, false, SurfaceFormat.Color);
          texture.SetData(new [] { Color.White });
                 Line_Creator.DrawLineTo(SpriteBatch,texture,new Vector2(_vaisseau.Position.X+_vaisseau.ShipAct.Width/2f,_vaisseau.Position.Y), new Vector2(_textRectangle.Left,_textRectangle.Bottom),Color.Green,1) ;
                 Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Right, _textRectangle.Bottom), Color.Green,1);
                }
                else
                {

                    texture = new Texture2D(Device, 5, 5, false, SurfaceFormat.Color);
                    texture.SetData(_colortable);
                    Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Left, _textRectangle.Bottom), Color.Green,5);
                    Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Right, _textRectangle.Bottom), Color.Green,5);   
                }
                }

            else if (! _isMultiplayer)
            {
                texture = new Texture2D(Device, 1, 1, false, SurfaceFormat.Color);
                texture.SetData(new[] { Color.White });
                Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Left, _textRectangle.Bottom), Color.Red,1);
                Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Right, _textRectangle.Bottom), Color.Red,1);
            }
            else
            {

                texture = new Texture2D(Device, 5, 5, false, SurfaceFormat.Color);
                texture.SetData(_colortable);
                Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Left, _textRectangle.Bottom), Color.Red,5);
                Line_Creator.DrawLineTo(SpriteBatch, texture, new Vector2(_vaisseau.Position.X + _vaisseau.ShipAct.Width / 2f, _vaisseau.Position.Y), new Vector2(_textRectangle.Right, _textRectangle.Bottom), Color.Red,5);
            }

            textbox.Draw();
            _vaisseau.Draw(SpriteBatch);
            if(!_isLaunched&&!textbox.HasFocus)
            {
                _LM.Draw(SpriteBatch);  
            }
         
            SpriteBatch.End();
         
            base.Draw(gameTime);
        }

        public void launchName()
        {
            _boxManager.Remove(_textRectangle);
            _textRectangle.Offset(0,-2);
            _boxManager.AddIgnored(_textRectangle);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {

            if (_LM.R2 == (_returnRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.go_back(_oldkeyboardState,_vaisseau,_LM);
            }

            if (_isLaunched)
            {
                launchName(); 

                if (_boxManager.CollisionRectangle(_textRectangle) == _Place1)
                {
                    _LM.R2 = Rectangle.Empty;
                    SauvegardesManager.SaveGameData data = new SauvegardesManager.SaveGameData();
                    if ( _isMultiplayer)
                    {
                        data.isMultiplayer = true;
                    }
                    else
                    {
                        data.isMultiplayer = false;
                    }
                    data.PlayerName = textbox.Text.ToString();
                    data.UnlockLvl = new bool[16];
                    data.UnlockLvl[0] = true;
                    data.nmPièces = 500;
                    data.perso = new State_game.Classes.Caracters.AmeliorationsJoueur();

                    for (int i = 1; i < 16; i++)
                    {
                        data.UnlockLvl[i] = false;
                    }
                    SauvegardesManager.CurrentSave = SauvegardesManager.SaveSlots.Save1;
                    SauvegardesManager.SaveGame(data, "saveslot" + 0 + ".sav");
                    StateManager.goto_state("selectionJoueur", Keyboard.GetState(), _vaisseau, _LM);
                }

                if (_boxManager.CollisionRectangle(_textRectangle) == _Place2)
                {
                    _LM.R2 = Rectangle.Empty;
                    SauvegardesManager.SaveGameData data = new SauvegardesManager.SaveGameData();
                    if (_isMultiplayer)
                    {
                        data.isMultiplayer = true;
                    }
                    else
                    {
                        data.isMultiplayer = false;
                    }
                    data.PlayerName = textbox.Text.ToString();
                    data.UnlockLvl = new bool[16];
                    data.UnlockLvl[0] = true;
                    data.nmPièces = 0;
                    data.perso = new State_game.Classes.Caracters.AmeliorationsJoueur();

                    for (int i = 1; i < 16; i++)
                    {
                        data.UnlockLvl[i] = false;
                    }
                    SauvegardesManager.CurrentSave = SauvegardesManager.SaveSlots.Save2;
                    SauvegardesManager.SaveGame(data, "saveslot" + 1 + ".sav");
                    StateManager.goto_state("selectionJoueur", Keyboard.GetState(), _vaisseau, _LM);
                }
                if (_boxManager.CollisionRectangle(_textRectangle) == _Place3)
                {
                    _LM.R2 = Rectangle.Empty;
                    SauvegardesManager.SaveGameData data = new SauvegardesManager.SaveGameData();
                    if (_isMultiplayer)
                    {
                        data.isMultiplayer = true;
                    }
                    else
                    {
                        data.isMultiplayer = false;
                    }
                    data.PlayerName = textbox.Text.ToString();
                    data.UnlockLvl = new bool[16];
                    data.UnlockLvl[0] = true;
                    data.nmPièces = 0;
                    data.perso = new State_game.Classes.Caracters.AmeliorationsJoueur();

                    for (int i = 1; i < 16; i++)
                    {
                        data.UnlockLvl[i] = false;
                    }
                    SauvegardesManager.CurrentSave = SauvegardesManager.SaveSlots.Save3;
                    SauvegardesManager.SaveGame(data, "saveslot" + 2 + ".sav");
                    StateManager.goto_state("selectionJoueur", Keyboard.GetState(), _vaisseau, _LM);
                }
            

            }
            _oldPosition=_vaisseau.Position;
            _vaisseau.Update(Device, _LM, Content, gameTime);
            
        
           if(_isLaunched==false)
           {
               if ( textbox.HasFocus==false && Keyboard.GetState().IsKeyDown(Keys.U) && _oldkeyboardState.IsKeyUp(Keys.U)) 
               {
                   _isMultiplayer=!_isMultiplayer;
               }
         
            if(_oldPosition!=_vaisseau.Position)
            {
                _boxManager.Remove(_textRectangle);
                _textRectangle.Offset((int)(_vaisseau.Position.X - _oldPosition.X), (int)(_vaisseau.Position.Y - _oldPosition.Y));
               _boxManager.AddIgnored(_textRectangle);
            }
           }
            textbox.Update(gameTime, _textRectangle, Keyboard.GetState(),_oldkeyboardState);
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && _oldkeyboardState.IsKeyUp(Keys.Space) && textbox.Text.Length != 0&&!textbox.HasFocus)
            {
                _isLaunched = true;
               
            }
            if (textbox.HasFocus == false && _isLaunched == false)
            {
                _LM.Update(_boxManager);
            }
            _oldkeyboardState = Keyboard.GetState();
           
            base.Update(gameTime, graphics);
        }

    }
}