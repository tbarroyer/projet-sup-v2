﻿using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    class Options : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//

        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;
        private VaisseauBase _vaisseau ;
        private Rectangle _VolumeRectangle;
        private Rectangle _ReturnRectangle;
        private Rectangle _FullscreenRectangle;

        private Texture2D back;
        private KeyboardState _oldkeyboardState;
        private string _VolumeString;
        private string _ReturnString;
        private string _FullscreenString;
        private Laser_Manager _LM;
        private Box_Manager _boxManager;

        //FONCTIONS

        public Options(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device,"options",content,spriteBatch)
        {
 
        }

        public override bool Init(KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
        {
            back = Content.Load<Texture2D>("backgroundmenu");
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _LM = LM;
            _oldkeyboardState = oldkeyboardState;
            _vaisseau = vaisseau;
            SpriteBatch = new SpriteBatch(Device);
             _VolumeRectangle=new Rectangle((int) (Device.Viewport.Width/19f),(int) (Device.Viewport.Height/5f),(int) (5*Device.Viewport.Width/19f),  (int) (Device.Viewport.Height/5f));
              _FullscreenRectangle = new Rectangle((int)(7*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
           _ReturnRectangle = new Rectangle((int)(13*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f), (int)(5*Device.Viewport.Width / 19f), (int)(Device.Viewport.Height / 5f));
            _VolumeString = "VOLUME";
            _FullscreenString = "PLEIN-ECRAN";
            _ReturnString = "RETOUR";
            _boxManager=new Box_Manager(Content);
            _boxManager.Add(_VolumeRectangle,Color.Green);
            _boxManager.Add(_ReturnRectangle,Color.Cyan);
            _boxManager.Add(_FullscreenRectangle,Color.BlanchedAlmond);
            return base.Init(oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), Color.White);
            _boxManager.DrawBox(Device, SpriteBatch);
            DrawString.DrawtheString(SpriteBatch, _font4, _VolumeString, _VolumeRectangle,Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _ReturnString, _ReturnRectangle,Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _FullscreenString, _FullscreenRectangle,Color.Black);
            _vaisseau.Draw(SpriteBatch);
            
            _LM.Draw(SpriteBatch);
    
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {

            if (_LM.R2 == (_VolumeRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("volume", Keyboard.GetState(), _vaisseau, _LM);

            }
            if (_LM.R2 == (_ReturnRectangle))
            {
                _LM.R2 = Rectangle.Empty;
                StateManager.goto_state("menu", Keyboard.GetState(), _vaisseau, _LM);

            }
            if (_LM.R2 == (_FullscreenRectangle))
            {

                _LM.R2 = Rectangle.Empty;
                if (graphics.IsFullScreen)
                {
                    graphics.IsFullScreen = false;
                    graphics.ApplyChanges();
                }
                else
                {
                    graphics.IsFullScreen=true;
                    graphics.ApplyChanges();

                }
            }
            

            _oldkeyboardState = Keyboard.GetState();
            _vaisseau.Update(Device, _LM, Content, gameTime);
            base.Update(gameTime, graphics);
            _LM.Update(_boxManager);
        }
    }
}