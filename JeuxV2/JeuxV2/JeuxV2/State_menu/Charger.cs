﻿using System.IO;
using JeuxV2.STATE_MANAGER;
using JeuxV2.State_menu.Vaisseau_J;
using JeuxV2.State_shoot.Manager;
using JeuxV2.Sauvegardes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    class Charger : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//


        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font4;
        private VaisseauBase _vaisseau;
        private Texture2D _arrow;

        private Texture2D back;
        private KeyboardState _oldkeyboardState;
        private Box_Manager _boxManager;
        private Rectangle _returnRectangle;
        private Laser_Manager _LM;
        private Rectangle _Place1;
        private string _place1s;
        private Rectangle _Place2;
        private string _place2s;
        private string _ReturnString;
        private Rectangle _Place3;
        private string _place3s;
        private Rectangle _Name;
        private string _TitleString;
        


        private bool[] tabSaves;

        //FONCTIONS
        public Charger(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "charger", content, spriteBatch)
        {
            type = "menu";
        }
        public override void Shutdown()
        {
           
        }
        public override bool Init(KeyboardState oldkeyboardState,VaisseauBase vaisseau,Laser_Manager LM)
        {
            tabSaves = SauvegardesManager.CheckSaves();
            back = Content.Load<Texture2D>("backgroundmenu");
            _font = Content.Load<SpriteFont>("Font/SpriteFont1");
            _font4 = Content.Load<SpriteFont>("Font/SpriteFont4");
            _vaisseau = vaisseau;
            _arrow = Content.Load<Texture2D>("Sprites/arrow");
            _TitleString = "Chargez un emplacement";
            _LM = LM;
            _boxManager = new Box_Manager(Content);
            _ReturnString = "RETOUR";
            _oldkeyboardState = oldkeyboardState;
            _Name = new Rectangle((int)(Device.Viewport.Width / 4f), (int)(Device.Viewport.Height / 15f), (int)(Device.Viewport.Width / 2f), (int)(Device.Viewport.Height / 9f));
            _returnRectangle = new Rectangle((int)(Device.Viewport.Width / 50f), (int)(Device.Viewport.Height / 10f),
                                             (int)(Device.Viewport.Width / 10f), (int)(Device.Viewport.Height / 15f));
            _Place1 = new Rectangle((int)(Device.Viewport.Width / 12f), (int)(Device.Viewport.Height / 5f), (int)(Device.Viewport.Width / 6f), (int)(Device.Viewport.Height / 5f));
            _Place2 = new Rectangle((int)(Device.Viewport.Width / 3f), (int)(Device.Viewport.Height / 5f), (int)(Device.Viewport.Width / 6f), (int)(Device.Viewport.Height / 5f));
            _Place3 = new Rectangle((int)(7 * Device.Viewport.Width / 12f), (int)(Device.Viewport.Height / 5f), (int)(Device.Viewport.Width / 6f), (int)(Device.Viewport.Height / 5f));
            _boxManager.Add(_returnRectangle, Color.LightGreen);
            _boxManager.Add(_Name, Color.LightGray);
            SpriteBatch = new SpriteBatch(Device);
            if (tabSaves[0])
            {

                _boxManager.Add(_Place1, Color.Green);
                _place1s = SauvegardesManager.LoadSave("saveslot0.sav").PlayerName;

            }
            else
            {
                _boxManager.Add(_Place1, Color.Gray);
                _place1s = "Emplacement 1";

            }
            if (tabSaves[1])
            {

                _boxManager.Add(_Place2, Color.Green);
                _place2s = SauvegardesManager.LoadSave("saveslot1.sav").PlayerName;

            }
            else
            {


                _boxManager.Add(_Place2, Color.Gray);
                _place2s = "Emplacement 2";

            }
            if (tabSaves[2])
            {

                _boxManager.Add(_Place3, Color.Green);
                _place3s = SauvegardesManager.LoadSave("saveslot2.sav").PlayerName;

            }
            else
            {


                _boxManager.Add(_Place3, Color.Gray);
                _place3s = "Emplacement 3";

            }

            return base.Init(_oldkeyboardState);

        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.White);

            SpriteBatch.Begin();
            SpriteBatch.Draw(back, new Vector2(0, 0), Color.White);
            _boxManager.DrawBox(Device, SpriteBatch);
            DrawString.DrawtheString(SpriteBatch, _font4, _ReturnString, _returnRectangle, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _TitleString, _Name, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _place1s, _Place1, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _place2s, _Place2, Color.Black);
            DrawString.DrawtheString(SpriteBatch, _font4, _place3s, _Place3, Color.Black);
   
       

           
            _vaisseau.Draw(SpriteBatch);
            _LM.Draw(SpriteBatch);
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (_LM.R2 == (_returnRectangle))
            {
                StateManager.goto_state("menu", _oldkeyboardState);
                //StateManager.go_back(_oldkeyboardState);

            }
            if (_LM.R2==_Place1)
            {
                _LM.R2 = Rectangle.Empty;
                if (tabSaves[0])
                {
                    
                
                SauvegardesManager.CurrentSave=SauvegardesManager.SaveSlots.Save1;
                
                StateManager.goto_state("selection", Keyboard.GetState());
                }
            }
            if (_LM.R2 == _Place2)
            {
                _LM.R2 = Rectangle.Empty;
                if (tabSaves[1])
                {
                    
               
                SauvegardesManager.CurrentSave = SauvegardesManager.SaveSlots.Save2;
                
                StateManager.goto_state("selection", Keyboard.GetState());
                }

            }
            if (_LM.R2 == _Place3)
            {
                _LM.R2 = Rectangle.Empty;
                if (tabSaves[2])
                {
                    
              
                SauvegardesManager.CurrentSave = SauvegardesManager.SaveSlots.Save3;
               
                StateManager.goto_state("selection", Keyboard.GetState());
                }

            }
            {
                
            }
            

            _oldkeyboardState = Keyboard.GetState();
            _vaisseau.Update(Device, _LM, Content, gameTime);
            _LM.Update(_boxManager);

            base.Update(gameTime, graphics);
        }

    }
}