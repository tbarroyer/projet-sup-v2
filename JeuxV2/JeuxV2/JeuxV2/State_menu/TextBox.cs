﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_menu
{
    internal class TextBox
    {
        private static Dictionary<Keys, char> characterByKey;

        public StringBuilder Text;
        public Vector2 position;
        public bool HasFocus;

        private GraphicsDevice graphicsDevice;
        private SpriteFont font;
        private SpriteBatch spriteBatch;
        private KeyboardState lastKeyboard;
        public Rectangle boundaries;
        private float xScale;
        private float yScale;
        SpriteEffects spriteEffects = new SpriteEffects();
        private bool renderIsDirty = true;
        private float scale;
        private Vector2 size;
        float rotation = 0.0f;
        Vector2 spriteOrigin = new Vector2(0, 0);
        float spriteLayer = 0.0f; // all the way in the front

        static TextBox()
        {
            characterByKey = new Dictionary<Keys, char>()
                #region keys

            {
                {Keys.A, 'a'},
                {Keys.B, 'b'},
                {Keys.C, 'c'},
                {Keys.D, 'd'},
                {Keys.E, 'e'},
                {Keys.F, 'f'},
                {Keys.G, 'g'},
                {Keys.H, 'h'},
                {Keys.I, 'i'},
                {Keys.J, 'j'},
                {Keys.K, 'k'},
                {Keys.L, 'l'},
                {Keys.M, 'm'},
                {Keys.N, 'n'},
                {Keys.O, 'o'},
                {Keys.P, 'p'},
                {Keys.Q, 'q'},
                {Keys.R, 'r'},
                {Keys.S, 's'},
                {Keys.T, 't'},
                {Keys.U, 'u'},
                {Keys.V, 'v'},
                {Keys.W, 'w'},
                {Keys.X, 'x'},
                {Keys.Y, 'y'},
                {Keys.Z, 'z'},
                {Keys.D0, '0'},
                {Keys.D1, '1'},
                {Keys.D2, '2'},
                {Keys.D3, '3'},
                {Keys.D4, '4'},
                {Keys.D5, '5'},
                {Keys.D6, '6'},
                {Keys.D7, '7'},
                {Keys.D8, '8'},
                {Keys.D9, '9'},
                {Keys.NumPad0, '0'},
                {Keys.NumPad1, '1'},
                {Keys.NumPad2, '2'},
                {Keys.NumPad3, '3'},
                {Keys.NumPad4, '4'},
                {Keys.NumPad5, '5'},
                {Keys.NumPad6, '6'},
                {Keys.NumPad7, '7'},
                {Keys.NumPad8, '8'},
                {Keys.NumPad9, '9'},
                {Keys.OemPeriod, '.'},
                {Keys.OemMinus, '-'},
                {Keys.Space, ' '}
            };

            #endregion
        }

        public TextBox(GraphicsDevice graphicsDevice,  SpriteFont font,Rectangle boundaries,SpriteBatch spriteBatch)
        {
            this.boundaries = boundaries;
            Text = new StringBuilder();
            size = font.MeasureString(Text);
            //size = font.MeasureString("dfgjlJL");
            position=new Vector2();
           
             xScale = (boundaries.Width / size.X);
             yScale = (boundaries.Height / size.Y);
            this.font = font;
             scale = Math.Min(xScale, yScale);
           
            var pp = graphicsDevice.PresentationParameters;
            int strWidth = (int)Math.Round(size.X * scale);
            int strHeight = (int)Math.Round(size.Y * scale);
            position.X = (((boundaries.Width - strWidth) / 2) + boundaries.X);
            position.Y = (((boundaries.Height - strHeight) / 2) + boundaries.Y);
            HasFocus = false;
            //renderTarget = new RenderTarget2D(graphicsDevice, width, height, false, pp.BackBufferFormat,pp.DepthStencilFormat);
            
            this.graphicsDevice = graphicsDevice;
             font.MeasureString(Text);
       
            characterByKey = new Dictionary<Keys, char>()
                #region keys

        
            {
                {Keys.A, 'a'},
                {Keys.B, 'b'},
                {Keys.C, 'c'},
                {Keys.D, 'd'},
                {Keys.E, 'e'},
                {Keys.F, 'f'},
                {Keys.G, 'g'},
                {Keys.H, 'h'},
                {Keys.I, 'i'},
                {Keys.J, 'j'},
                {Keys.K, 'k'},
                {Keys.L, 'l'},
                {Keys.M, 'm'},
                {Keys.N, 'n'},
                {Keys.O, 'o'},
                {Keys.P, 'p'},
                {Keys.Q, 'q'},
                {Keys.R, 'r'},
                {Keys.S, 's'},
                {Keys.T, 't'},
                {Keys.U, 'u'},
                {Keys.V, 'v'},
                {Keys.W, 'w'},
                {Keys.X, 'x'},
                {Keys.Y, 'y'},
                {Keys.Z, 'z'},
                {Keys.D0, '0'},
                {Keys.D1, '1'},
                {Keys.D2, '2'},
                {Keys.D3, '3'},
                {Keys.D4, '4'},
                {Keys.D5, '5'},
                {Keys.D6, '6'},
                {Keys.D7, '7'},
                {Keys.D8, '8'},
                {Keys.D9, '9'},
                {Keys.NumPad0, '0'},
                {Keys.NumPad1, '1'},
                {Keys.NumPad2, '2'},
                {Keys.NumPad3, '3'},
                {Keys.NumPad4, '4'},
                {Keys.NumPad5, '5'},
                {Keys.NumPad6, '6'},
                {Keys.NumPad7, '7'},
                {Keys.NumPad8, '8'},
                {Keys.NumPad9, '9'},
                {Keys.OemPeriod, '.'},
                {Keys.OemMinus, '-'},
                {Keys.Space, ' '}
            };

                #endregion

            this.spriteBatch = spriteBatch;
        }
        public void UpdateText()
        {
            size = font.MeasureString(Text);
             xScale = (boundaries.Width / size.X);
             yScale = (boundaries.Height / size.Y);
             scale = Math.Min(xScale, yScale);
             int strWidth = (int)Math.Round(size.X * scale);
             int strHeight = (int)Math.Round(size.Y * scale);

             position.X = (((boundaries.Width - strWidth) / 2) + boundaries.X);
             position.Y = (((boundaries.Height - strHeight) / 2) + boundaries.Y);
        }

        public void Update(GameTime Time,Rectangle boundaries,KeyboardState state,KeyboardState oldstate)
        {
            if (state.IsKeyDown(Keys.F1) && oldstate.IsKeyUp(Keys.F1))
                HasFocus = !HasFocus;
            if (!HasFocus)
            {
                this.boundaries = boundaries;
                UpdateText();
               
                return;
            }
            this.boundaries = boundaries;
            UpdateText();
            var keyboard = Keyboard.GetState();

            foreach (var key in keyboard.GetPressedKeys())
            {
                if (!lastKeyboard.IsKeyUp(key))
                {
                    continue;
                }

                if (key == Keys.Delete || key == Keys.Back)
                {
                    if (Text.Length == 0)
                    {
                        continue;
                    }
                    Text.Length--;
                    renderIsDirty = true;
                    continue;
                }

                char character;
                if (!characterByKey.TryGetValue(key, out character))
                {
                    continue;
                }
                if (keyboard.IsKeyDown(Keys.LeftShift) ||
                keyboard.IsKeyDown(Keys.RightShift))
                {
                    character = Char.ToUpper(character);
                }
                Text.Append(character);
                renderIsDirty = true;
            }

            lastKeyboard = keyboard;
        }

        public void PreDraw()
        {
            if (!renderIsDirty)
            {
                return;
            }
            renderIsDirty = false;
            //var existingRenderTargets = graphicsDevice.GetRenderTargets();
           // graphicsDevice.SetRenderTarget(renderTarget);
           // spriteBatch.Begin();
           // graphicsDevice.Clear(BackgroundColor);
          //  spriteBatch.DrawString(
            //    font, Text,
            //    Vector2.Zero, ForegroundColor);
           // spriteBatch.End();
           // graphicsDevice.SetRenderTargets(existingRenderTargets);
           
        }

        public void Draw()
        {
        
            spriteBatch.DrawString(font, Text, position, Color.YellowGreen, rotation, spriteOrigin, scale, spriteEffects, spriteLayer);
            //SpriteBatch.Draw(renderTarget, Position, Color.White);
    
        }
    }
}
