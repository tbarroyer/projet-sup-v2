using System;
using Microsoft.Xna.Framework;

namespace JeuxV2
{
#if WINDOWS || XBOX
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(string[] args)
        {
            using (Base game = new Base())
            {
                if (args.Length == 0)
                {

                    {
                        game.Run();

                    }
                }




            }
        }
    }
}

#endif
