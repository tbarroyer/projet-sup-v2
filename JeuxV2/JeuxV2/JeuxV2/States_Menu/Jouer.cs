﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace StateManager
{
    class Jouer : State
    {
        //============================================//
        //============================================//
        //======FOR INFORMATION CHECK MENU.CS=========//
        //============================================//
        //============================================//


        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font2;

        private Texture2D _arrow;
        private Vector2 _posArrow;
        private KeyboardState _oldkeyboardState;
        private string _currentSelection;

        //FONCTIONS
        public Jouer(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "jouer", content, spriteBatch)
        {
        }

        public override bool Init(KeyboardState oldkeyboardState)
        {
            _font = _content.Load<SpriteFont>("Font/SpriteFont1");
            _font2 = _content.Load<SpriteFont>("Font/SpriteFont2");

            _arrow = _content.Load<Texture2D>("Sprites/arrow");
            _currentSelection = "gamep";
            _posArrow = new Vector2(5, 70);

            _oldkeyboardState = oldkeyboardState;

            _spriteBatch = new SpriteBatch(_device);

            return base.Init(_oldkeyboardState);
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }

        public override void Draw(GameTime gameTime)
        {
            _device.Clear(Color.White);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_arrow, _posArrow, Color.White);
            _spriteBatch.DrawString(_font, "Jouer", new Vector2(10, 20), Color.Black);
            _spriteBatch.DrawString(_font2, "Nouvelle Partie", new Vector2(50, 70), Color.Black);
            _spriteBatch.DrawString(_font2, "Retour", new Vector2(50, 110), Color.Black);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && _oldkeyboardState.IsKeyUp(Keys.Enter))
            {
                STATE_MANAGER.goto_state(_currentSelection, Keyboard.GetState());
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down) && _oldkeyboardState.IsKeyUp(Keys.Down))
            {
                switch (_currentSelection)
                {
                    case "gamep":
                        _posArrow.Y += 40;
                        _currentSelection = "menu";
                        break;
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Up) && _oldkeyboardState.IsKeyUp(Keys.Up))
            {
                switch (_currentSelection)
                {
                    case "menu":
                        _posArrow.Y -= 40;
                        _currentSelection = "gamep";
                        break;
                }
            }

            _oldkeyboardState = Keyboard.GetState();

            base.Update(gameTime);
        }
    }
}