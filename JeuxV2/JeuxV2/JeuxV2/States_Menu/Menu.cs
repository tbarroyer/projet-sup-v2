﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace StateManager
{
    class Menu : State
    {
        //VARIABLES
        private SpriteFont _font;
        private SpriteFont _font2;

        private Texture2D _arrow; //Arrows texture
        private Vector2 _posArrow; //Arrows position
        private KeyboardState _oldkeyboardState; //old keystate
        private string _currentSelection;


        //FONCTIONS
        public Menu(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "menu", content, spriteBatch)
        {
        }

        public override bool Init(KeyboardState oldkeyboardState)
        {
            _font = _content.Load<SpriteFont>("Font/SpriteFont1"); //loading font1
            _font2 = _content.Load<SpriteFont>("Font/SpriteFont2"); //loading font2

            _arrow = _content.Load<Texture2D>("Sprites/arrow");

            _currentSelection = "jouer"; //Value of first selection
            _posArrow = new Vector2(5, 70); //First position of arrow
            _oldkeyboardState = oldkeyboardState; //Getting oldkeystate

            _spriteBatch = new SpriteBatch(_device);

            return base.Init(oldkeyboardState);
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }

        public override void Draw(GameTime gameTime)
        {
            _device.Clear(Color.White);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_arrow, _posArrow, Color.White); //Draw the arrow
            _spriteBatch.DrawString(_font, "Menu", new Vector2(10, 20), Color.Black);
            _spriteBatch.DrawString(_font2, "Jouer", new Vector2(50, 70), Color.Black);
            _spriteBatch.DrawString(_font2, "Options", new Vector2(50, 110), Color.Black);
            _spriteBatch.DrawString(_font2, "Quitter", new Vector2(50, 150), Color.Black);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && _oldkeyboardState.IsKeyUp(Keys.Enter))
            {
                STATE_MANAGER.goto_state(_currentSelection, Keyboard.GetState()); //Changing the state to the selected state
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down) && _oldkeyboardState.IsKeyUp(Keys.Down))
            {
                switch (_currentSelection)
                {
                    case "jouer":
                        _posArrow.Y += 40; //Moving the arrow
                        _currentSelection = "options"; //Changing the selected state
                        break;
                    case "options":
                        _posArrow.Y += 40; //Moving the arrow
                        _currentSelection = "quitter"; //Changing the selected state
                        break;
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Up) && _oldkeyboardState.IsKeyUp(Keys.Up))
            {
                switch (_currentSelection)
                {
                    case "quitter":
                        _posArrow.Y -= 40; //Moving the arrow
                        _currentSelection = "options"; //Changing the selected state
                        break;
                    case "options":
                        _posArrow.Y -= 40; //Moving the arrow
                        _currentSelection = "jouer"; //Changing the selected state
                        break;
                }
            }

            _oldkeyboardState = Keyboard.GetState(); //Oldstate is now Newstate

            base.Update(gameTime);
        }
    }
}
