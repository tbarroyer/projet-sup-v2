﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_shoot.Explosionnm
{
    public class Explosion
    {
        private Texture2D[] tabText;
        private Vector2 _pos;
        private int compt;

        public Explosion(ContentManager content, Vector2 pos)
        {
            _pos = pos;

            tabText = new Texture2D[5];

            for (int i = 0; i < 5; i++)
            {
                tabText[i] = content.Load<Texture2D>("Explosions/s" + (i+1));
            }

            compt = 0;
        }

        public void Draw(SpriteBatch sb)
        {
            if (compt <= 4)
                sb.Draw(tabText[0], _pos, Color.White);
            else if (compt <= 8)
                sb.Draw(tabText[1], _pos, Color.White);
            else if (compt <= 12)
                sb.Draw(tabText[2], _pos, Color.White);
            else if (compt <= 16)
                sb.Draw(tabText[3], _pos, Color.White);
            else if (compt <= 20)
                sb.Draw(tabText[4], _pos, Color.White);

            compt++;
        }

    }
}
