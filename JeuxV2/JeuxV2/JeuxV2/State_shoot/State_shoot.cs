﻿using JeuxV2.STATE_MANAGER;
using JeuxV2.State_shoot.Manager;
using JeuxV2.State_shoot.Map;
using JeuxV2.State_shoot.VaisseauJ;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace JeuxV2.State_shoot
{
    class state_shoot : State
    {
        //VARIABLES
        VaisseauBase ship1;
        Ennemis_Manager MM_E;
        Laser_Manager LM_J;
        map map;
        
        private KeyboardState _oldkeyboardState;


        //FONCTIONS
        public state_shoot(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
            : base(device, "state_shoot", content, spriteBatch)
        {
        }

        public override bool Init(KeyboardState oldkeyboardState, string level, int IDfile)
        {
            _oldkeyboardState = oldkeyboardState;

            SpriteBatch = new SpriteBatch(Device);

            ship1 = new VaisseauBase(Content);
            MM_E = new Ennemis_Manager();
            LM_J = new Laser_Manager();
            map = new map(level, Content, IDfile);
            
            return base.Init(_oldkeyboardState);
        }

        public override void Draw(GameTime gameTime)
        {
            Device.Clear(Color.Black);
            map.draw(SpriteBatch);
            SpriteBatch.Begin();
            ship1.Draw(SpriteBatch);
            MM_E.Draw(SpriteBatch);
            LM_J.Draw(SpriteBatch);
            SpriteBatch.End();

            if (Keyboard.GetState().IsKeyDown(Keys.R))
            {
                StateManager.goto_state("selection", Keyboard.GetState()); //Back to main menu
            }

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            
            if (ship1.Vie != 0)
            {
                map.Update(MM_E, Content, ship1);
                LM_J.Update(MM_E, Content, map, ship1);
                MM_E.Update(ship1, Content, map);
                ship1.Update(graphics, LM_J, Content,gameTime);
            }
            base.Update(gameTime, graphics);
        }

    }
}