﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using JeuxV2.State_shoot.Manager;

namespace JeuxV2.State_shoot.VaisseauE
{
    class Ennemi4 : vaisseauE
    {
        ContentManager _content;
        int compt;
        bool mouv;
        private bool typeshoot;

         public Ennemi4(Vector2 _position, ContentManager content)
        {
            _content = content;
            position = _position;
            speed = 2;
            ship_e = content.Load<Texture2D>("Ennemis/4");
            compt = 0;
            vie = 40;
            mouv = false;
            typeshoot = false;
        }

         public override void Update(Laser_Manager manalaser, Ennemis_Manager E_M, ContentManager content)
         {
             compt++;
             if (position.X > 800 - ship_e.Width)
             {
                 mouv = true;
                 typeshoot = !typeshoot;
             }
                 
             if (position.X < 0)
                 mouv = false;

             if(!mouv)
             {
                 position.X += 5;
             }
             if (mouv)
             {
                 position.X -= 5;
             }

             if (compt > 50)
             {
                 if (typeshoot)
                        manalaser.Add(new Vector2(position.X + ship_e.Width / 2, position.Y + ship_e.Height), _content, -14, Color.Red);
                 else
                        E_M.Add(new Vector2(position.X, position.Y + ship_e.Height), content, 2);
                 compt = -30;
             }

             if (Position.Y > 10)
             {
                 speed = 0;
             }
             
             base.Update(manalaser, E_M, content);
         } 
    }
}
