﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using JeuxV2.State_shoot.Manager;

using JeuxV2.State_shoot.Lasers;
using Microsoft.Xna.Framework.Content;

namespace JeuxV2.State_shoot.VaisseauE
{
    public class vaisseauE
    {
        protected int vie;
        public int Vie
        {
            get
            {
                return vie;
            }
        }

        protected Vector2 position;
        public Vector2 Position
        {
            get
            {
                return position;
            }
            
        }


        protected Texture2D ship_e;
        public Texture2D Ship_e
        {
            get
            {
                return ship_e;
            }

        }

        protected int speed;

        protected int HP;

        public virtual void Update(Laser_Manager manalaser, Ennemis_Manager E_M, ContentManager content)
        {
            position.Y += speed;
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(ship_e, position, Color.White);
        }

        public void perdrevie()
        {
            vie--;
        }
    }
}
