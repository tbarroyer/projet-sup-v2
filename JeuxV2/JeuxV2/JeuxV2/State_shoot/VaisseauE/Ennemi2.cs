﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using JeuxV2.State_shoot.VaisseauE;
using JeuxV2.State_shoot.Manager;

namespace JeuxV2.State_shoot.Manager
{
    class Ennemi2 : vaisseauE
    {
        ContentManager _content;
        int compt;

         public Ennemi2(Vector2 _position, ContentManager content)
        {
            _content = content;
            position = _position;
            speed = 2;
            ship_e = content.Load<Texture2D>("Ennemis/2");
            compt = 0;
            vie = 1;
        }

         public override void Update(Laser_Manager manalaser, Ennemis_Manager E_M, ContentManager content)
         {
             compt++;
             if (compt > 30)
             {
                   manalaser.Add(new Vector2(position.X + ship_e.Width / 2, position.Y + ship_e.Height), _content, -7, Color.Red);
                   compt = -30;
             }
             
             base.Update(manalaser, E_M, content);
         } 
    }
}
