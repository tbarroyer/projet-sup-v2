﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_shoot.VaisseauE
{
    public class Ennemi1 : vaisseauE
    {
        public Ennemi1(Vector2 _position, ContentManager content)
        {
            position = _position;
            speed = 3;
            ship_e = content.Load<Texture2D>("Ennemis/1");
            vie = 1;
        }
    }
}
