﻿using System;
using JeuxV2.State_shoot.Manager;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using JeuxV2.State_shoot.Bonusnm;
using JeuxV2.State_shoot.VaisseauJ;

namespace JeuxV2.State_shoot.Map
{
    public class map
    {
        private int[,] tab;

        private int compt;

        public int ligne;
        private int idfile;

        public int Idfile
        {
            get { return idfile; }
        }

        private Texture2D back;
        private Vector2 posBack;

        private Texture2D end;

        private List<Bonus> listeBonus;

        public map(string file, ContentManager content, int id)
        {
            ligne = 99;
            compt = 0;
            tab = new int[100,20];
            Load_map(file);
            back = content.Load<Texture2D>("Levels/Levels_shoot/back");
            posBack = new Vector2(0, 600 - back.Height);
            listeBonus = new List<Bonus>();
            idfile = id;

            end = content.Load<Texture2D>("win");
        }

        public void Update(Ennemis_Manager enn, ContentManager content, vaisseauJ joueur)
        {
            if (compt == 40 && ligne > 0)
            {
                MapReader(enn, content);
                ligne--;
                compt = 0;
                
            }
            else compt++;

            if (ligne == 0)
            {
                Sauvegardes.SauvegardesManager.SaveGameData data = Sauvegardes.SauvegardesManager.LoadCurrentSave();
                data.UnlockLvl[idfile + 1] = true;
                data.nmPièces += joueur.nmPieces;
                Sauvegardes.SauvegardesManager.SaveCurrentSave(data);
                
                if (Keyboard.GetState().IsKeyDown(Keys.R))
                    STATE_MANAGER.StateManager.goto_state("selection", Keyboard.GetState());
            }

            if (ligne != 0)
                posBack.Y += 5;

            foreach (Bonus item in listeBonus)
            {
                item.Update(joueur);
            }
        }

        private void MapReader(Ennemis_Manager enn, ContentManager content)
        {
            for (int i = 0; i < 20; i++)
            {
                if (tab[ligne, i] != 0)
                {
                    if (tab[ligne, i] < 6)
                        enn.Add(new Vector2(i * 40, 0), content, tab[ligne, i]);
                    else if (tab[ligne, i] == 6)
                        listeBonus.Add(new Shield(new Vector2(i * 40, 0), content));
                    else if (tab[ligne, i] == 7)
                        listeBonus.Add(new Piece(new Vector2(i * 40, 0), content));
                }
            }
        }

        private void Load_map(string file)
        {
            StreamReader streamreader = new StreamReader(Path.GetFullPath("Content/Levels/Levels_shoot/" + file));

            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    tab[i, j] = (int)(Char.GetNumericValue((char)streamreader.Read()));
                    streamreader.Read();
                }
                streamreader.Read();
            }
        }

        public void draw(SpriteBatch sb)
        {
            sb.Begin();

            if (posBack.Y > 600)
                posBack.Y = 0 - back.Height;

            sb.Draw(back, posBack, Color.White);
            

            foreach (Bonus item in listeBonus)
            {
                item.Draw(sb);
            }

            if (ligne == 0)
            {
                sb.Draw(end, Vector2.Zero, Color.White);
            }
            
            sb.End();
        }

    }
}
