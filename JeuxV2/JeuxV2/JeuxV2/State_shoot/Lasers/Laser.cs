﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace JeuxV2.State_shoot.Lasers
{
    public class Laser
    {
        private Color myColor;

        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        private int speed;

        private Texture2D sprite_laser;
        public Texture2D Sprite_laser
        {
            get { return sprite_laser; }
        }

        public Laser(Vector2 _position, int _speed, ContentManager content, Color mycolor)
        {
            sprite_laser = content.Load<Texture2D>("Lasers/laser1");
            position = _position;
            position.X -= sprite_laser.Width / 2;
            position.Y -= sprite_laser.Height / 2;
            speed = _speed;
            myColor = mycolor;
        }
        public void Update()
        {
            position.Y -= speed;
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(sprite_laser, position, myColor);
        }


    }
}
