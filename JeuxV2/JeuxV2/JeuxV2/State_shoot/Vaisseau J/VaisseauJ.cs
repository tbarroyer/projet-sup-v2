﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using JeuxV2.State_shoot.Manager;
using JeuxV2.State_shoot.Lasers;

namespace JeuxV2.State_shoot.VaisseauJ
{
    public class vaisseauJ
    {
        protected struct particule
        {
            public Vector2 pos;
            public int compt;
        }

        public struct shield
        {
            public int compteur;
            public bool isActivate;
            public int timeur;
            public Texture2D sprite;
        }

        public int nmPieces;

        public int Myshieldcompteur
        {
            get { return myshield.compteur; }
            set { myshield.compteur = value; }
        }

        protected shield myshield;

        protected Texture2D imgParticule;

        protected particule[] myParticulesTab;


        protected Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }


        protected Texture2D shipAct;

        public Texture2D ShipAct
        {
            get { return shipAct; }
        }

        protected int vie;

        public int Vie
        {
            get { return vie; }
        }


        public Texture2D game_over;

        protected SpriteFont font;

        protected Texture2D[] ship;

        protected int time_laps;
        protected int laser_speed;

        protected int speed;

        protected int comptL;

        protected int comptR;

        public void Init(ContentManager content)
        {
            nmPieces = 0;


            LoadGameOver(content);

            font = content.Load<SpriteFont>("Font/SpriteFont2");

            comptL = 0;
            comptR = 0;

            myParticulesTab = new particule[30];

            imgParticule = content.Load<Texture2D>("Explosions/particule");

            Random r = new Random();

            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt = r.Next(30);
                myParticulesTab[i].pos = new Vector2();
                myParticulesTab[i].pos.X = position.X + r.Next(-10, 10);
                myParticulesTab[i].pos.Y = position.Y + r.Next(10);
            }

            myshield = new shield();
            myshield.compteur = 0;
            myshield.isActivate = false;
            myshield.sprite = content.Load<Texture2D>("bouclier");
        }

        public void LoadGameOver(ContentManager content)
        {
            game_over = content.Load<Texture2D>("gameover");
        }

        public void Update(GraphicsDeviceManager graphics, Laser_Manager LM, ContentManager content, GameTime time)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (time_laps >= 500)
                {
                    if (Sauvegardes.SauvegardesManager.LoadCurrentSave().perso.Dblaser)
                    {
                        LM.Add(new Vector2(position.X + shipAct.Width / 2 + 10, position.Y - shipAct.Height), content, laser_speed, Color.White);
                        LM.Add(new Vector2(position.X + shipAct.Width / 2 - 10, position.Y - shipAct.Height), content, laser_speed, Color.White);
                    }
                    else
                    {
                        LM.Add(new Vector2(position.X + shipAct.Width / 2, position.Y - shipAct.Height), content, laser_speed, Color.White);
                    }
                    time_laps = 0;
                }
            }
            time_laps += (int) time.ElapsedGameTime.TotalMilliseconds;
            deplacement();
            collision(graphics);

            Random r = new Random();

            shieldUpdate();
           
            for (int i = 0; i < 30; i++)
            {
                myParticulesTab[i].compt++;

                int t = 10;
                t += r.Next(-7, 7);

                if (myParticulesTab[i].compt > t)
                {
                    myParticulesTab[i].pos.X = position.X + 11;
                    myParticulesTab[i].pos.Y = position.Y + shipAct.Height - 4;
                    myParticulesTab[i].compt = 0;
                }

                myParticulesTab[i].pos.X += r.Next(-5, 5);
                myParticulesTab[i].pos.Y += 5;

            }
        }

        public void Draw(SpriteBatch sb)
        {
            if (vie == 0)
                sb.Draw(game_over, new Vector2(0, 0), Color.White);
            sb.DrawString(font, "Vie : " + System.Convert.ToString(vie), new Vector2(40, 8), Color.White);

            foreach (var particule in myParticulesTab)
            {
                sb.Draw(imgParticule, particule.pos, Color.White);
            }

            sb.Draw(shipAct, position, Color.White);

            if (myshield.isActivate)
                sb.Draw(myshield.sprite, new Vector2(position.X - 15, position.Y - 15), Color.White);

        }

        private void shieldUpdate()
        {
            if (myshield.isActivate == true)
            {
                myshield.timeur += 1;

                if (myshield.timeur == 600)
                    myshield.isActivate = false;
            }


            if ((Keyboard.GetState().IsKeyDown(Keys.B)) && (myshield.compteur >= 5))
            {
                myshield.compteur -= 5;
                myshield.isActivate = true;
                myshield.timeur = 0;
            }
        }

        private void deplacement()
        {
            shipAct = ship[0];



            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                position.X -= speed;
                shipAct = ship[1];
                comptL++;

                if (comptL > 10)
                {
                    shipAct = ship[2];
                }
            }
            else comptL = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                position.X += speed;
                shipAct = ship[3];
                comptR++;
                if (comptR > 10)
                {
                    shipAct = ship[4];
                }
            }
            else comptR = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                position.Y -= speed;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                position.Y += speed;
            }
        }

        private void collision(GraphicsDeviceManager graphics)
        {
            if (position.Y > graphics.PreferredBackBufferHeight - shipAct.Height)
            {
                position.Y = graphics.PreferredBackBufferHeight - shipAct.Height;
            }

            if (position.Y < 0)
            {
                position.Y = 0;
            }

            if (position.X > graphics.PreferredBackBufferWidth - shipAct.Width)
            {
                position.X = graphics.PreferredBackBufferWidth - shipAct.Width;
            }

            if (position.X < 0)
            {
                position.X = 0;
            }
        }

        public void perdrevie()
        {
            if (!myshield.isActivate)
            {
                if (vie != 0)
                    vie--;
            }
            else myshield.isActivate = false;
                
        }

        public bool CollisionlaserJ(Laser m, vaisseauJ joueur)
        {
            if (!((m.Position.X >= joueur.Position.X + joueur.shipAct.Width)
                  || (m.Position.X + m.Sprite_laser.Width <= joueur.Position.X)
                  || (m.Position.Y >= joueur.Position.Y + joueur.shipAct.Height)
                  || (m.Position.Y + m.Sprite_laser.Height <= joueur.Position.Y)
                ))
            {
                joueur.perdrevie();
                return true;
            }
            return false;
        }
    }
}
