﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace JeuxV2.State_shoot.VaisseauJ
{
        public class VaisseauStrong : vaisseauJ
        {
            public VaisseauStrong(ContentManager content)
            {
                ship = new Texture2D[5];
                ship[0] = content.Load<Texture2D>("Ship/Strong/ship1");
                ship[1] = content.Load<Texture2D>("Ship/Strong/ship1L");
                ship[2] = content.Load<Texture2D>("Ship/Strong/ship1L2");
                ship[3] = content.Load<Texture2D>("Ship/Strong/ship1R");
                ship[4] = content.Load<Texture2D>("Ship/Strong/ship1R2");
                shipAct = ship[0];
                vie = 4;
                speed = 6;

                position = new Vector2((800 - shipAct.Width) / 2, 400);

                base.Init(content);
            }
        }
    
}
