﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using JeuxV2.State_shoot.VaisseauJ;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JeuxV2.State_shoot.Bonusnm
{
    class Shield : Bonus
    {
        public Shield(Vector2 Pos, ContentManager content)
        {
            pos = Pos;
            sprite = content.Load<Texture2D>("Sprites/Power Up/power2");
            isnotActivate = false;
        }

        public override void Action(vaisseauJ joueur)
        {
            isnotActivate = true;
            joueur.Myshieldcompteur++;
        }
    }
}
