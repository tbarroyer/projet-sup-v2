﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JeuxV2.State_shoot.VaisseauJ;

namespace JeuxV2.State_shoot.Bonusnm
{
    class Bonus
    {
        protected Texture2D sprite;
        protected bool grosbullshit;
        protected Vector2 pos;
        protected bool isnotActivate;

        public virtual void Action(vaisseauJ joueur) { }

        public void Update(vaisseauJ joueur)
        {
            pos.Y += 4;
            Collision(joueur);
        }

        private void Collision(vaisseauJ joueur)
        {
            if ((joueur.Position.X >= pos.X + sprite.Width)
                || (joueur.Position.X + joueur.ShipAct.Width <= pos.X)
                || (joueur.Position.Y >= pos.Y + sprite.Height)
                || (joueur.Position.Y + joueur.ShipAct.Height <= pos.Y)
                )
            {

            } //PAS DE COLLISION
            else
            {
                if (!isnotActivate)
                    Action(joueur);

                grosbullshit = true;
            }
        }

        public void Draw(SpriteBatch sb)
        {
            if (!grosbullshit)
            {
                sb.Draw(sprite, pos, Color.White);
            }            
        }
    }
}
