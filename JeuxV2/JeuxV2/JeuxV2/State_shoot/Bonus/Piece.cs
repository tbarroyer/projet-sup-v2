﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeuxV2.State_shoot.VaisseauJ;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace JeuxV2.State_shoot.Bonusnm
{
    class Piece : Bonus
    {
         public Piece(Vector2 Pos, ContentManager content)
        {
            pos = Pos;
            sprite = content.Load<Texture2D>("Sprites/Power Up/Coins/Coin1");
            isnotActivate = false;
        }

        public override void Action(vaisseauJ joueur)
        {
            joueur.nmPieces++;
        }
    }
}
