﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using JeuxV2.State_shoot.Map;
using JeuxV2.State_shoot.VaisseauE;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JeuxV2.State_shoot.Lasers;
using JeuxV2.State_shoot.VaisseauJ;
using JeuxV2.State_shoot.Explosionnm;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace JeuxV2.State_shoot.Manager
{
    public class Ennemis_Manager
    {
        private List<vaisseauE> list_e;
        private Laser_Manager manalaser;
        private List<Explosion> explList; 

        public Ennemis_Manager()
        {
            list_e = new List<vaisseauE>();
            manalaser = new Laser_Manager();
            explList = new List<Explosion>();
        }

        public void Draw(SpriteBatch sb)
        {
            foreach (vaisseauE l in list_e)
            {
                l.Draw(sb);
            }

            foreach (Explosion explosion in explList)
            {
                explosion.Draw(sb);
            }

            manalaser.Draw(sb);
        }

        public void Update(vaisseauJ joueur, ContentManager content, map m)
        {
            for (int i = 0; i < list_e.Count; i++)
            {
                list_e.ElementAt(i).Update(manalaser, this, content);
            }

            manalaser.Update(joueur, content);

            Collision(joueur, content);
        }

        public void Add(Vector2 _position, ContentManager content, int type)
        {
            switch (type)
            {
                case 1:
                    list_e.Add(new Ennemi1(_position, content));
                    break;
                case 2:
                    list_e.Add(new Ennemi2(_position, content));
                    break;
                case 3:
                    list_e.Add(new Ennemi3(_position, content));
                    break;
                case 4:
                    list_e.Add(new Ennemi4(_position, content));
                    break;
            }
                    
            
        }

        private void Collision(vaisseauJ joueur, ContentManager content)
        {
            bool test = false;

            vaisseauE remo = new vaisseauE(); 

            foreach (vaisseauE l in list_e)
            {
                if ((joueur.Position.X >= l.Position.X + l.Ship_e.Width)
                    || (joueur.Position.X + joueur.ShipAct.Width <= l.Position.X)
                    || (joueur.Position.Y >= l.Position.Y + l.Ship_e.Height)
                    || (joueur.Position.Y + joueur.ShipAct.Height <= l.Position.Y)
                    )
                {
                   
                } //PAS DE COLLISION
                else
                {
                    remo = l;
                    test = true;
                    joueur.perdrevie();
                }
            }

            if (test)
            {
                explList.Add(new Explosion(content, remo.Position));

                if (joueur.Vie <= 0)
                    explList.Add(new Explosion(content, joueur.Position));

                list_e.Remove(remo);
            }
                
        }

        public bool Collisionlaser(ContentManager content, Laser m, map map, vaisseauJ joueur)
        {
            bool test = false;

            vaisseauE remo = new vaisseauE(); 

            foreach (vaisseauE l in list_e)
            {
                if (!((m.Position.X >= l.Position.X + l.Ship_e.Width)
                    || (m.Position.X + m.Sprite_laser.Width <= l.Position.X)
                    || (m.Position.Y >= l.Position.Y + l.Ship_e.Height)
                    || (m.Position.Y + m.Sprite_laser.Height <= l.Position.Y)
                    ))
                {
                    remo = l;
                    test = true;
                }
            }

            if (test == true)
            {
                remo.perdrevie();

                if (remo.Vie <= 0)
                {
                    explList.Add(new Explosion(content, remo.Position));
                    list_e.Remove(remo);

                    if (remo is Ennemi4)
                    {
                        Sauvegardes.SauvegardesManager.SaveGameData data = Sauvegardes.SauvegardesManager.LoadCurrentSave();
                        data.UnlockLvl[map.Idfile + 1] = true;
                        Sauvegardes.SauvegardesManager.SaveCurrentSave(data);
                        data.nmPièces += joueur.nmPieces;
                        map.ligne = 0;
                    }
                }
                
            }
                
            return test;
        }
        

    }
}
