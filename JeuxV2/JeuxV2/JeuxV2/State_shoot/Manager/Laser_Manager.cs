﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeuxV2.State_menu;
using JeuxV2.State_shoot.Map;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JeuxV2.State_shoot.Lasers;
using JeuxV2.State_shoot.VaisseauJ;
using Microsoft.Xna.Framework.Content;

namespace JeuxV2.State_shoot.Manager
{
    public class Laser_Manager
    {
    
        private List<Laser> list_e;
     // private List<Rectangle> list_r; 
      //public List<Rectangle> List_r
      //{
          //get { return list_r; }
        //}

        public Rectangle R2
        {
        get;
        set;
   
        }
     // public Rectangle r()
       //{

        //foreach ( Rectangle rect  in list_r)
          //  {
             // return rect;
           //}
 //           return Rectangle.Empty;

   //     }
        public Laser_Manager()
        {
            list_e = new List<Laser>();
        }

        public void Draw(SpriteBatch sb)
        {
            foreach (Laser l in list_e)
            {
                l.Draw(sb);
            }
        }

        public void Update(Ennemis_Manager LM, ContentManager content, map m, vaisseauJ joueur)
        {
            foreach (Laser l in list_e)
            {
                l.Update();
            }
            Collision(LM, content, m, joueur);
        }
        public void Update(vaisseauJ joueur, ContentManager content)
        {
            foreach (Laser l in list_e)
            {
                l.Update();
            }
            Collision(joueur, content);
        }
        public void Update(Box_Manager BM)
        {
            foreach (Laser l in list_e)
            {
                l.Update();
            }
            Collision(BM);
  
        }
        public void Update()
        {
            foreach (Laser l in list_e)
            {
                l.Update();
            }


         
                
            }
            public void Clear()

            {
                list_e.Clear();
            }
    
        public void Add(Vector2 _position, ContentManager content, int speed, Color mycolor)
        {
            list_e.Add(new Laser(_position, speed, content, mycolor));
        }
        private void Collision(Box_Manager BM)
        {
            List<Laser> listr = new List<Laser>();

            foreach (Laser l in list_e)
            {
                
                if (BM.Collisionlaser(l)!=Rectangle.Empty)
                    listr.Add(l);
                   R2=(BM.Collisionlaser(l));

            }

            foreach (Laser l in listr)
            {
                list_e.Remove(l);
            }
        }

        private void Collision(Ennemis_Manager LM, ContentManager content, map m, vaisseauJ joueur)
        {
           List<Laser> listr = new List<Laser>();

            foreach (Laser l in list_e)
            {
                if (LM.Collisionlaser(content, l, m, joueur))
                    listr.Add(l);
            }

            foreach (Laser l in listr)
            {
                list_e.Remove(l);
            }
        }

        private void Collision(vaisseauJ joueur, ContentManager content)
        {
            List<Laser> listr = new List<Laser>();

            foreach (Laser l in list_e)
            {
                if (joueur.CollisionlaserJ(l, joueur))
                    listr.Add(l);
            }

            foreach (Laser l in listr)
            {
                list_e.Remove(l);
            }
        }
    }
}
