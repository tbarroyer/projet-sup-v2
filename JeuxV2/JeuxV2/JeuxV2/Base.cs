using JeuxV2.State_game;
using JeuxV2.State_menu;
using JeuxV2.State_shoot;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using JeuxV2.STATE_MANAGER;
using Microsoft.Xna.Framework.Media;

namespace JeuxV2
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Base : Game
    {
        Video vid;
        VideoPlayer vidPlayer;

        Texture2D vidTexture;
        Rectangle vidRectangle;
        int level ;
        Song song;
        SpriteBatch _spriteBatch;
        public GraphicsDeviceManager Graphics { get; set; }

        public Base()
        {
            Graphics = new GraphicsDeviceManager(this);
            Graphics.IsFullScreen = false;
            Graphics.PreferredBackBufferHeight = 1080;
            Graphics.PreferredBackBufferWidth = 1920;
            Content.RootDirectory = "Content";
            Graphics.ApplyChanges();
        }
        
        

        protected override void Initialize()
        {
            vidPlayer = new VideoPlayer();

            //Adding all states
            StateManager.add_state(new Options(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Menu(GraphicsDevice, Content, _spriteBatch,this));
            StateManager.add_state(new Jouer(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Gamep(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Charger(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Nouvelle(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Selection(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new state_shoot(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Achat(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new SelectionJoueur(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Volume(GraphicsDevice,Content,_spriteBatch) );
            StateManager.add_state(new Arcade(GraphicsDevice, Content, _spriteBatch));
            StateManager.add_state(new Gamep2j(GraphicsDevice, Content, _spriteBatch));
            //going to first state
            StateManager.goto_state("menu", Keyboard.GetState());
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            vid = Content.Load<Video>("vid");
            vidRectangle = new Rectangle(GraphicsDevice.Viewport.X, GraphicsDevice.Viewport.Y, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

            if (level== 0)
                vidPlayer.Play(vid);

            StateManager.Init(Keyboard.GetState());
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            switch (level)
            {
                case 0:
                    if (vidPlayer.State == MediaState.Stopped)
                        level++;
                    break;

                case 1:
                    vidPlayer.Stop();
                    song = Content.Load<Song>("Sound/song");
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
                    level++;
                    break;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                level ++;

            StateManager.Update(gameTime, Graphics);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            StateManager.Draw(gameTime);
            vidTexture = vidPlayer.GetTexture();
            _spriteBatch.Begin(); 

            switch (level)
            {
                case 0:
                   _spriteBatch.Draw(vidTexture, vidRectangle, Color.White);
                    break;

                case 1:
                    break;

            }
         
            _spriteBatch.End();
            base.Draw(gameTime);    
        }
    }
}
