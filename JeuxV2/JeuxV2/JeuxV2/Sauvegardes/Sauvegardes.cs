﻿using System;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using JeuxV2.State_game.Classes.Caracters;

namespace JeuxV2.Sauvegardes
{
    public static class SauvegardesManager
    {
        public enum SaveSlots
        {
            Save1,
            Save2,
            Save3
        }

        [Serializable]
        public struct SaveGameData
        {
            public string PlayerName;
            public bool[] UnlockLvl;
            public int nmPièces;
            public AmeliorationsJoueur perso;
            public bool isMultiplayer;
        }

        private static StorageDevice _storageDevice;

        private static SaveSlots _currentSave;
        public static SaveSlots CurrentSave
        {
            get { return _currentSave; }
            set { _currentSave = value; }
        }

        static SauvegardesManager()
        {
            IAsyncResult result = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
            _storageDevice = StorageDevice.EndShowSelector(result);
        }

        private static StorageContainer GetContainer(string name)
        {
            IAsyncResult result = _storageDevice.BeginOpenContainer(name, null, null);
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = _storageDevice.EndOpenContainer(result);

            result.AsyncWaitHandle.Close();

            return container;
        }

        public static void SaveGame(SaveGameData data, string name)
        {
            StorageContainer container = GetContainer("Saves1");

            if (container.FileExists(name))
                container.DeleteFile(name);

            Stream stream = container.CreateFile(name);
            
            XmlSerializer serializer = new XmlSerializer(typeof(SaveGameData));
            serializer.Serialize(stream, data);
            stream.Close();

            container.Dispose();
        }

        public static SaveGameData LoadSave(string name)
        {
            StorageContainer container = GetContainer("Saves1");

            // Pas de sauvegarde
            if (!container.FileExists(name))
                return new SaveGameData();

            // La sauvegarde existe
            Stream stream = container.OpenFile(name, FileMode.Open);

            XmlSerializer serializer = new XmlSerializer(typeof(SaveGameData));

            SaveGameData config = (SaveGameData)serializer.Deserialize(stream);

            stream.Close();

            // Ne pas oublier de fermer le flux ET le container
            container.Dispose();

            return config;
        }

        public static SaveGameData LoadCurrentSave()
        {
            string name = getNameCurrentSave();

            StorageContainer container = GetContainer("Saves1");

            // Pas de sauvegarde
            if (!container.FileExists(name))
                return new SaveGameData();

            // La sauvegarde existe
            Stream stream = container.OpenFile(name, FileMode.Open);

            XmlSerializer serializer = new XmlSerializer(typeof(SaveGameData));

            SaveGameData config = (SaveGameData)serializer.Deserialize(stream);

            stream.Close();

            // Ne pas oublier de fermer le flux ET le container
            container.Dispose();

            return config;
        }

        public static void SaveCurrentSave(SaveGameData data)
        {
            StorageContainer container = GetContainer("Saves1");

            string name = getNameCurrentSave();

            if (container.FileExists(name))
                container.DeleteFile(name);

            Stream stream = container.CreateFile(name);

            XmlSerializer serializer = new XmlSerializer(typeof(SaveGameData));
            serializer.Serialize(stream, data);
            stream.Close();

            container.Dispose();
        }

        public static bool[] CheckSaves()
        {
            bool[] toReturn = new bool[3];

            StorageContainer container = GetContainer("Saves1");
            
            for (int i = 0; i < 3; i++)
            {
                if (container.FileExists("saveslot" + i + ".sav"))
                    toReturn[i] = true;
                else toReturn[i] = false;
            }

            return toReturn;
        }

        private static string getNameCurrentSave()
        {
            string name = "";

            switch (_currentSave)
            {
                case SaveSlots.Save1:
                    name = "saveslot0.sav";
                    break;
                case SaveSlots.Save2:
                    name = "saveslot1.sav";
                    break;
                case SaveSlots.Save3:
                    name = "saveslot2.sav";
                    break;
            }

            return name;
        }
        
    }
}
